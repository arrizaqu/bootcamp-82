package com.xsis.day_5;
import java.util.Scanner;


	public class Soal_3 {
		private static Scanner sc;
		//set array
		public String[][] setData(int n)
		{
			String[][] data = new String[2*n-1][n];
			int max = n-1;
			int tengah = max/2;
			for (int y=0; y<n;y++){
				int a = 1;
				for(int x=0; x<n;x++){
					if(x-y<=tengah && x+y>=tengah)// atas 
					{
					data[x][y]= a+"\t";
					a++; }
					}
				}
			return data;		
		}
		public void showData(int n){
			System.out.println("==== Menampilkan data : "+ n+ " ====");
			String[][] data = setData((n*2)-1);
			for (int y=0; y<n;y++){
				for (int x=0;x<n*2-1;x++){
					if(data[x][y]==null)
					{
						data[x][y] = "\t";
							}
							System.out.print(data[x][y]+ "\t");
						}
						System.out.println();
					}
				}
		
		public static void main(String args[]){
			 sc = new Scanner(System.in);
			 System.out.print("masukkan angka:  ");
		     int n = sc.nextInt();
			Soal_3 logic =new Soal_3();
			logic.showData(n);//sub routine
		}

	}
