package com.xsis.day_3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class soal_1 {
	
	//set array
	public int[] setData(int n){
		int[] data = new int[n];
		
		//set code
		int x;
		for(x=0; x<n; x++){
			if(x<2)
				data[x]=1;
			else
			data[x]=data[x-1]+data[x-2];
			
		}
		
		
		return data;
		
	}
	
	//show array
	public void showData(int n){
		System.out.print("Fibonaci:  ");
		int[] data = setData(n);
		for(int y=0; y<n; y++){
			
				
				System.out.print(data[y] + " ");
				
			
			//System.out.println();
		}
	}
	
	public static void main(String[] args) throws IOException{
		soal_1 logic = new soal_1();
		//logic.showData(9);
		
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		System.out.print("Masukkan jumlah Angka fibonaci = ");
		String angka = br.readLine();
		int n = Integer.parseInt(angka);
		logic.showData(n);
		
	}
}
