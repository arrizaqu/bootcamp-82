package com.xsis.day_3;

import java.util.Scanner;
	public class Soal_9 {
		private static Scanner sc;
		//set array
		 public int[] getFib(int n){
		        
		        int[] data = new int[n];
		        int a=1;
		        int b=0;
		        for(int y = 0; y < n; y++){
		           
		            if(y%2==1){ 
		                data[y] = 0;
		            } 
		            else {
		            	data[y] = a;
		            	a=a+b;
		    			b=a-b;
		               }
		            
		            
		        }
		        
		        return data;
		    }
		 public String[][] setData(int n){
		        String[][] data = new String[n][n];
		        int[] fib1 = getFib(n);
		        
		        for(int temp : fib1){
		           System.out.println(""+ temp);
		        }
		        
		        int max = n - 1;
		        
		        for(int y = 0; y < n; y++){
		        		
		            for(int x= 0; x < n; x++){
		                
		               if( y + x >= max && x <= y && y%2==0) {
		                       // if (y%2==0){
		                                data[x][y]=  fib1[n-y-1] + "\t";
		                       // }

		                }
		                else if(y+x<=max && x>=y && y%2==0) //Atas
		                {
		                        //if (y%2==0){
		                                data[x][y]= fib1[y]+"\t";
		                       // }
		                }
		                else if(y+x<=max && x<=y && x%2==0) //Kiri
		                {
		                      //  if (x%2==0){
		                                data[x][y]= fib1[x]+"\t";
		                      //  }
		                }
		                else if(y+x>=max && x>=y && x%2==0) //Kanan
		                {
		                        data[x][y]= fib1[n-x-1] + "\t";
		                } 
		            }
		            
		        }       
		        
		        return data;
		        
		 }
		public void showData(int n){
			System.out.println("jumlah data: "+ n + "=====");
			String [][] data = setData(n);
			for(int y=0; y<n;y++){
				for(int x=0;x<n;x++){
					if(data [x][y] == null){
						data[x][y] ="\t";
					}
					System.out.print(data[x][y]);
					
				}
				System.out.println("");
			}
			
		}
		
		public static void main(String args[]){
			 sc = new Scanner(System.in);
			 System.out.print("masukkan angka:  ");
		     int n = sc.nextInt();
			Soal_9 logic =new Soal_9();
			logic.showData(n);//sub routine
		}

}
