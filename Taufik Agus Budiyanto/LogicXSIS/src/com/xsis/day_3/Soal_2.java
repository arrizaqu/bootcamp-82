package com.xsis.day_3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Soal_2 {
	
	//set array
	public int[] setData(int n){
		int[] data = new int[n];
		
		//set code
		int x;
		for(x=0; x<n; x++){
			if(x<3)
				data[x]=1;
			else
			data[x]=data[x-1]+data[x-2]+data[x-2];
			
		}
		
		
		return data;
		
	}
	
	//show array
	public void showData(int n){
		System.out.print("Menampilkan fibonaci \n");
		int[] data = setData(n);
		for(int y=0; y<n; y++){
			
				
				System.out.print(data[y] + " ");
				
			
			//System.out.println();
		}
	}
	
	public static void main(String[] args) throws IOException{
		Soal_2 logic2 = new Soal_2();
		//logic.showData(9);
		
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		System.out.print("Masukkan Angka fibonaci = ");
		String angka = br.readLine();
		int n = Integer.parseInt(angka);
		logic2.showData(n);
		
	}
}
