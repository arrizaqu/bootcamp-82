package com.xsis.day_3;
import java.util.Scanner ;
public class Soal_5 {
	private static Scanner sc;

	public static void main(String[] args) {
		sc = new Scanner (System.in);
		
		System.out.print(" Masukkan batas bilangan: ") ;
		
		int a = sc.nextInt();

	    // int input = 100;

	     for (int i = 2; i <a; i++) {
	         boolean isPrima = true;

	         for (int j = 2; j < i; j++) {
	             if(i%j==0){
	                 isPrima = false;
	                 break;
	             }
	         }

	         if(isPrima==true){
	         System.out.print(i+" ");
	    }
	  }
   }
}