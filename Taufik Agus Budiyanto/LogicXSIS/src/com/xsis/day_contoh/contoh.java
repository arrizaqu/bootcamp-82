package com.xsis.day_contoh;

public class contoh {
	 public int[] getFib(int n){
	        
	        int[] data = new int[n];
	        int loop = 1;
	        for(int y = 0; y < n; y++){
	           
	            if(y < 2){ 
	                data[y] = 1;
	            } else {
	                data[y] = data[y - 1] + data[y - 2];
	            }
	            
	            loop++;
	        }
	        
	        return data;
	    }
	    
	    public String[][] setData(int n){
	        String[][] data = new String[n][n];
	        int[] fib1 = getFib(n);
	        
	        for(int temp : fib1){
	            System.out.println(""+ temp);
	        }

}
	    }