package com.xsis.day_5;

public class Soal_9 {
	// set array
	//rumus Ganjil
	public int[] getGanjil(int n){
		int[] data = new int[4*n-3];
		for(int y = 0;y < 4*n-3;y++){
			if(y <= 2*n-2){
				if(y == 0){
					data[y] = 1;
				}else{
					data[y] = data[y - 1] + 2;
				}
			}else{
				data[y]=data[4*n-3-y-1];
			}
		}
	return data;
	}
	public String[][] setData(int n){
		String[][] data = new String[4*n-3][2*n-1];
		int[] gjl = getGanjil(n);
		
		//code set
		int max = n - 1;
		int total = 2*max;													//batastengahdanakhirbelah ketupat 1
		int max2 = 3*max;
		int total2 = 4*max;													//batastengahdanakhirbelah ketupat 2
		for(int y = 0; y < 2*n-1; y++){
			int i = 0;
			int j = 0;
			for(int x = 0; x < 4*n-3; x++){
																			//belah ketupat 1	
				if( y + x >= max && x < max && y <= max){					//boxkiriatas
					data[x][y] = gjl[i]+ "\t";
					i++;
				}else if( x - y <= max  && x >= max && y <= max){			//boxkananatas
					data[x][y] = gjl[i]+ "\t";	
					i--;				
				}else if( y - x <= max  && x < max && y >= max){			//boxkiribawah
					data[x][y] = gjl[i]+ "\t";	
					i++;				
				}else if( x + y <= max+total  && x >= max && y >= max){		//boxkananbawah
					data[x][y] = gjl[i]+ "\t";	
					i--;													
				}
																			//belah ketupat 2
				if( y + x >= max2 && x < max2 && y <= max){					//boxkiriatas				
					data[x][y] = gjl[j]+ "\t";
					j++;
				}else if( x - y <= max2  && x >= max2 && y <= max){			//boxkananatas					
					data[x][y] = gjl[j]+ "\t";	
					j--;				
				}else if( x - y >= n-1  && x < max2 && y >= max && x>total){//boxkiribawah					
					data[x][y] = gjl[j]+ "\t";	
					j++;				
				}else if( x + y <= max + total2  && x >= max2 && y >= max){	//boxkananbawah
					data[x][y] = gjl[j]+ "\t";	
					j--;										
				}
			}
		}
	return data;
	}
	// show array
	public void showData(int n){		
		String[][] data = setData(n);		
		for(int y = 0; y < 2*n-1; y++){
			for(int x = 0; x< 4*n-3; x++){
				if(data[x][y]==null){
					data[x][y]="\t";
				}
				System.out.print(data[x][y]);
			}
			System.out.println("\t");
		}
	}	
	public static void main(String args[]){
	Soal_9 array = new Soal_9();
	array.showData(5);
	}
}
