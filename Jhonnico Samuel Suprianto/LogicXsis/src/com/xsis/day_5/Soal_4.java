package com.xsis.day_5;

public class Soal_4 {
	// set array
		//rumus BilanganBulat
	public int[] getInt(int n){
		int[] data = new int[2*n-1];
		for(int y = 0;y < 2*n-1;y++){
			if(y == 0){
				data[y] = 1;
			}else{
				data[y] = data[y - 1] + 1;
			}
		}
	return data;
	}
	public String[][] setData(int n){
		String[][] data = new String[2*n-1][n];
		int[] int1 = getInt(n);
		
		//code set
		int max = n - 1;
		int total = 2*max;
		for(int y = 0; y < n; y++){
			int i=0;
			for(int x = 0; x < 2*n-1; x++){
				if( x >= y && x + y <= total){
					data[x][y] = int1[i]+ "\t";
					i++;						
				}
			}
		}
	return data;
	}
	// show array
	public void showData(int n){		
		String[][] data = setData(n);		
		for(int y = 0; y < n; y++){
			for(int x = 0; x< 2*n-1; x++){
				if(data[x][y]==null){
					data[x][y]="\t";
				}
				System.out.print(data[x][y]);
			}
			System.out.println("\t");
		}
	}	
	public static void main(String args[]){
	Soal_4 array = new Soal_4();
	array.showData(5);
	}
}
