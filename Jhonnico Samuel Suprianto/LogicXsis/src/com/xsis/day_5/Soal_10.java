package com.xsis.day_5;

public class Soal_10 {
	// set array
	//rumus Fibo
	public int[] getFib(int n){
		int[] data = new int[4*n-3];
		for(int y = 0;y < 4*n-3;y++){
			if(y <= 2*n-2){
				if(y < 2){
					data[y] = 1;
				}else{
					data[y] = data[y - 1] + data[y-2];
				}
			}else{
				data[y]=data[4*n-3-y-1];
			}
		}
	return data;
	}
	public String[][] setData(int n){
		String[][] data = new String[4*n-3][2*n-1];
		int[] fib1 = getFib(n);
		
		//code set
		int max = n - 1;														//batastengahbox1
		int total = 2*max;														//batasakhirbox1
		int max2 = 3*max;														//batastengahbox2
		int total2 = 4*max;														//batasakhirbox2	
		for(int y = 0; y < 2*n-1; y++){
			int i = 0;
			int j = 0;
			for(int x = 0; x < 4*n-3; x++){
				if( x % 2 == 0 && y % 2 == 1 || x % 2 == 1 && y % 2 == 0){		//selang-seling
					data[x][y]="\t";
				}else if( y + x >= max && x - y <= max && y < max){				//boxatasketupat1
					data[x][y] = fib1[n-y-1]+ "\t";
				}else if( y - x <= max  && x + y <= max + total && y > max){	//boxbawahketupat1
					data[x][y] = fib1[y-max]+ "\t";																		
				}else if( y + x >= max2 && x - y <= max2 && y < max){			//boxatasketupat2				
					data[x][y] = fib1[n-y-1]+ "\t";
				}else if( x - y >= n-1  && x + y <= max + total2 && y > max ){	//boxbawahketupat2					
					data[x][y] = fib1[y-max]+ "\t";	
				}else if( y == max ){											//Fibonachi tengah
					if( x < max ){
						data[x][y] = fib1[i]+ "\t";
						i++;
					}else if( x >= max && x < total){
						data[x][y] = fib1[i]+ "\t";
						i--;
					}else if( x >= total && x < max2){
						data[x][y] = fib1[j]+ "\t";
						j++;
					}else if( x >= max2 && x <= total2){
						data[x][y] = fib1[j]+ "\t";
						j--;
					}
				}
			}
		}
	return data;
	}
	// show array
	public void showData(int n){		
		String[][] data = setData(n);		
		for(int y = 0; y < 2*n-1; y++){
			for(int x = 0; x< 4*n-3; x++){
				if(data[x][y]==null){
					data[x][y]="\t";
				}
				System.out.print(data[x][y]);
			}
			System.out.println("\t");
		}
	}	
	public static void main(String args[]){
	Soal_10 array = new Soal_10();
	array.showData(5);
	}
}
