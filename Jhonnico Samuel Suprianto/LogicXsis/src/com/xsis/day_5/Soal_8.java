package com.xsis.day_5;

public class Soal_8 {
	// set array
	//rumus GanjilBelahKetupat
	public int[] getGbk(int n){
		int[] data = new int[2*n-1];
		for(int y = 0;y < 2*n-1;y++){
			if(y <= n-1){
				if(y == 0){
					data[y] = 1;
				}else{
					data[y] = data[y - 1] + 2;
				}
			}else{
				data[y]=data[2*n-1-y-1];
			}
		}
	return data;
	}
	public String[][] setData(int n){
		String[][] data = new String[2*n-1][2*n-1];
		int[] gbk = getGbk(n);
		
		//code set
		int max = n - 1;
		int total = 2*max;			
		for(int y = 0; y < 2*n-1; y++){
			int i = 0;
			for(int x = 0; x < 2*n-1; x++){
				if( y + x >= max && x < max && y <= max){
					data[x][y] = gbk[i]+ "\t";
					i++;
				}else if( x - y <= max  && x >= max && y <= max){
					data[x][y] = gbk[i]+ "\t";	
					i--;				
				}
				else if( y - x <= max  && x < max && y >= max){
					data[x][y] = gbk[i]+ "\t";	
					i++;			
				}else if( x + y <= max + total  && x >= max && y >= max){
					data[x][y] = gbk[i]+ "\t";	
					i--;				
				}
			}
		}
	return data;
	}
	// show array
	public void showData(int n){		
		String[][] data = setData(n);		
		for(int y = 0; y < 2*n-1; y++){
			for(int x = 0; x< 2*n-1; x++){
				if(data[x][y]==null){
					data[x][y]="\t";
				}
				System.out.print(data[x][y]);
			}
			System.out.println("\t");
		}
	}	
	public static void main(String args[]){
	Soal_8 array = new Soal_8();
	array.showData(5);
	}


}
