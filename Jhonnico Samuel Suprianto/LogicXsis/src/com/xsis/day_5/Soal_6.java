package com.xsis.day_5;

public class Soal_6 {
	// set array
	//rumus GanjilNaikTurun
	public int[] getGnt(int n){
		int[] data = new int[2*n-1];
		for(int y = 0;y < 2*n-1;y++){
			if(y <= n-1){
				if(y == 0){
					data[y] = 1;
				}else{
					data[y] = data[y - 1] + 2;
				}
			}else{
				data[y]=data[2*n-1-y-1];
			}
		}
	return data;
	}
	public String[][] setData(int n){
		String[][] data = new String[2*n-1][n];
		int[] gjl1 = getGnt(n);
		//code set
		int max = n - 1;		
		//int total = 2*max;
		for(int y = 0; y < n; y++){
			int i=0;
			for(int x = 0; x < 2*n-1; x++){
				if( y + x >= max && x < max){
					data[x][y] = gjl1[i]+ "\t";
					i++;
				}else if( x - y <=max && x >= max){					
					data[x][y] = gjl1[i]+ "\t";
					i--;
				}
			}
		}
	return data;
	}
	// show array
	public void showData(int n){		
		String[][] data = setData(n);		
		for(int y = 0; y < n; y++){
			for(int x = 0; x< 2*n-1; x++){
				if(data[x][y]==null){
					data[x][y]="\t";
				}
				System.out.print(data[x][y]);
			}
			System.out.println("\t");
		}
	}	
	public static void main(String args[]){
	Soal_6 array = new Soal_6();
	array.showData(6);
	}
}
