package com.xsis.day_3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Soal_3_1 {
	public void latihan() {
		int n, m, c, a, b;
		Scanner dataInput = new Scanner(System.in);
		System.out.print("Masukkan bilangan pertama : ");
		n = dataInput.nextInt();
		System.out.print("Masukkan bilangan kedua : ");
		m = dataInput.nextInt();
		System.out.println();
		int FPB=0;
		int KPK;
		for(a=1;a<=n;a++){
			b = n%a;
			c = m%a;
			if(b==c && b==0 && c==0){
				FPB=a;
			}
		}
		KPK = (n*m)/FPB;
		System.out.print("KPK dari "+ n +", "+ m +" adalah: "+ KPK);
	}
	
			
	public static void main(String args[]){
	Soal_3_1 kpk = new Soal_3_1();
	kpk.latihan();
	}
}
