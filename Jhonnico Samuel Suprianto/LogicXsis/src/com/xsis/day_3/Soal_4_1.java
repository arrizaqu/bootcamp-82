package com.xsis.day_3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Soal_4_1 {
	public void latihan()throws IOException{
		int a;
		int b;
		boolean prima=true;
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader Br = new BufferedReader(isr);
		System.out.println("Masukkan Angka");
		String angka1 = Br.readLine();
		a = Integer.parseInt(angka1);
		for (b=2;b<a;b++){
			if((a%b)==0){
				prima=false;
				break;
			}
		}if (prima){
			System.out.println(a+" merupakan bilangan prima");
		}else{
			System.out.println(a+" bukan bilangan prima");
		}
	}
	
	public static void main(String args[]) throws IOException {
	Soal_4_1 bilprima = new Soal_4_1();
	bilprima.latihan();
	}

}
