package com.xsis.day_3;

public class Soal_8 {
	// set array
		//rumus Fibo
	public int[] getFib(int n){
		int[] data = new int[n];
		for(int y = 0;y < n;y++){
			if(y == 0){
				data[y] = 1;
			}else{
				data[y] = data[y - 1] + 1;
			}
		}
	return data;
	}
	
	public String[][] setData(int n){
		String[][] data = new String[n][n];
		int[] fib1 = getFib(n);
		//code set
		int max = n - 1;
		for(int y = 0; y < n; y++){
			for(int x = 0; x < n; x++){
				if( y + x >= max && x <= y){
					if(y%2==0){
					data[x][y] = fib1[n-y-1]+ "\t";
					}
					}else if( y + x <= max && x >= y){
						if(y%2==0){
					data[x][y] = fib1[y]+ "\t";
						}
					}else if( y + x <= max && x<=y){
						if(x%2==0){
					data[x][y] = fib1[x]+ "\t";
						}
					}else if( y + x >= max && x>=y){
						if(x%2==0){
					data[x][y] = fib1[n-x-1]+ "\t";
						}
					}
			}
		}		
	return data;
	}
	// show array
	public void showData(int n){		
		String[][] data = setData(n);
		for(int y = 0; y < n; y++){
			for(int x = 0; x< n; x++){
				if(data[x][y]==null){
					data[x][y]="\t";
				}
				System.out.print(data[x][y]);
			}
			System.out.println("\t");
		}
	}		
	public static void main(String args[]){
	Soal_8 array = new Soal_8();
	array.showData(9);
					/*InputStreamReader isr = new InputStreamReader(System.in);
					BufferedReader Br = new BufferedReader(isr);
					System.out.println("Masukkan Angka");
					String angka = Br.readLine();
					int n = Integer.parseInt(angka);
					System.out.print(n);					
					array.showData(n);*/
	}
}
