package com.xsis.day_3;


public class Soal_6 {
	// set array
	public int[] setData(int n){
		int[] data = new int[n];
		//code set
		for(int x=0;x<n;x++){
			if(x<2){
				data[x]=1;
			}else{
				data[x]=data[x-1]+data[x-2];
			}
		}							
	return data;
	}
	// show array
	public void showData(int n){
	System.out.println("===Menampilkan data :"+ n + "===");
	int[] data = setData(n);	
		for(int y=0;y<n;y++){
			for(int x=0;x<n;x++){
				if((x==y)||(x+y==n-1)){
					System.out.print(data[x]+"\t");
				}else if((x>y)&&(x+y<n-1)){
					System.out.print("A\t");
				}else if((x>y)&&(x+y>n-1)){
					System.out.print("B\t");
				}else if((x<y)&&(x+y>n-1)){
					System.out.print("C\t");
				}else{
					System.out.print("D\t");
				}
			}System.out.println("\t");
		}
	}		
	public static void main(String ags[]){
	Soal_6 array = new Soal_6();
	array.showData(9);
	}
}