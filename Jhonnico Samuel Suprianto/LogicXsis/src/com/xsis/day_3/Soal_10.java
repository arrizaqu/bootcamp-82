package com.xsis.day_3;

public class Soal_10 {
	// set array
	//rumus Fibo
	public char[] getAlphabet(){
		char[] huruf = new char[26];
		for(char c = 'A'; c <= 'Z'; ++c){
			huruf[c-'A'] = c;
		}
		return huruf;
	}
	
	public String[] getFib(int n){
		String [] data = new String[n];
		int angka1=1;
		int angka2=0;
		char[] huruf = getAlphabet();
		int i=0;
		for(int y = 0;y < n;y++){
			if(y % 2 == 1){
				data[y] = huruf[i] +"";
				i++;
			}else{
				data[y] = angka1 +"";
				angka1 = angka1 + angka2;
				angka2 = angka1 - angka2;
			}
		}
		return data;
	}
	
	
	public String[][] setData(int n){
		String[][] data = new String[n][n];
		String[] fib1 = getFib(n);
		//code set
		int max = n - 1;
		for(int y = 0; y < n; y++){
			for(int x = 0; x < n; x++){
				if( y + x >= max && x <= y){
					data[x][y] = fib1[n-y-1]+ "\t";
				}else if( y + x <= max && x >= y){
					data[x][y] = fib1[y]+ "\t";
				}else if( y + x <= max && x<=y){
					data[x][y] = fib1[x]+ "\t";
				}else if( y + x >= max && x>=y){
					data[x][y] = fib1[n-x-1]+ "\t";
				}				
			}
		}		
	return data;
	}
	// show array
	public void showData(int n){		
		String[][] data = setData(n);
		for(int y = 0; y < n; y++){
			for(int x = 0; x< n; x++){
				if(data[x][y]==null){
					data[x][y]="\t";
				}
				System.out.print(data[x][y]);
			}
			System.out.println("\t");
		}
	}		
	public static void main(String args[]){
	Soal_10 array = new Soal_10();
	array.showData(20);
					/*InputStreamReader isr = new InputStreamReader(System.in);
					BufferedReader Br = new BufferedReader(isr);
					System.out.println("Masukkan Angka");
					String angka = Br.readLine();
					int n = Integer.parseInt(angka);
					System.out.print(n);					
					array.showData(n);*/
	}
}
