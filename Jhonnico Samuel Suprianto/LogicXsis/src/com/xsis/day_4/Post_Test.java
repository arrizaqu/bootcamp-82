package com.xsis.day_4;

public class Post_Test {
	// set array
			//rumus Fibo
		public int[] getFib(int n){
			int[] data = new int[n];
			for(int y = 0;y < n;y++){
				if(y < 2){
					data[y] = 1;
				}else{
					data[y] = data[y - 1] + data[y - 2];
				}
			}
		return data;
		}
		
		public String[][] setData(int n){
			String[][] data = new String[2*n-1][n];
			int[] fib1 = getFib(n);
			//code set
			int max = n - 1;
			int total = 2*max;
			for(int y = 0; y < n; y++){
				for(int x = 0; x < 2*n-1; x++){
					if( y + x == max || x == y){
						data[x][y] = fib1[x]+ "\t";
					}else if( x - y == max || x + y == total){
						data[x][y] = fib1[total-x]+ "\t";
					}
				}
			}
		return data;
		}
		// show array
		public void showData(int n){		
			String[][] data = setData(n);		
			for(int y = 0; y < n; y++){
				for(int x = 0; x< 2*n-1; x++){
					if(data[x][y]==null){
						data[x][y]="\t";
					}
					System.out.print(data[x][y]);
				}
				System.out.println("\t");
			}
		}		
		public static void main(String args[]){
		Post_Test array = new Post_Test();
		array.showData(9);
						/*InputStreamReader isr = new InputStreamReader(System.in);
						BufferedReader Br = new BufferedReader(isr);
						System.out.println("Masukkan Angka");
						String angka = Br.readLine();
						int n = Integer.parseInt(angka);
						System.out.print(n);					
						array.showData(n);*/
		}

}
