package com.xsis.training;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Array2 {
	// set array
		public String[][] setData(int n){
			String[][] data = new String[n][n];
			//code set
			int max = 8;
			int nt = max/2;
			for(int y=0;y<n;y++){
				for(int x=0;x<n;x++){
					if(x==y){
						data[x][y] = x +"."+ y +"\t";
					}else if(x+y==max){
						data[x][y] = x +"."+ y +"\t";
					}else if(x==0){
						data[x][y] = x +"."+ y +"\t";
					}else if(y==0){
						data[x][y] = x +"."+ y +"\t";
					}else if(x==nt){
						data[x][y] = x +"."+ y +"\t";
					}else if(y==nt){
						data[x][y] = x +"."+ y +"\t";
					}else if(x==max){
						data[x][y] = x +"."+ y +"\t";
					}else if(y==max){
						data[x][y] = x +"."+ y +"\t";
					}
				}	
			}
			
			return data;
		}
		
		// show array
		public void showData(int n){
			System.out.println("===Menampilkan data :"+ n + "===");
			String[][] data = setData(n);
			
			for(int y=0;y<n;y++){
				for(int x=0;x<n;x++){
					if(data[x][y]==null){
						data[x][y]="\t";
					}
					System.out.print(data[x][y]);
				}
				System.out.println("\t");
			}
			
		}
		
		
		public static void main(String ags[]) throws IOException{
			Array2 array = new Array2();
			array.showData(9);
			//InputStreamReader isr = new InputStreamReader(System.in);
			//BufferedReader Br = new BufferedReader(isr);
			//System.out.println("Masukkan Angka");
			//String angka = Br.readLine();
			//int n = Integer.parseInt(angka);
			//System.out.print(n);
			
			//array.showData(n);
		}
}
