package com.xsis.training;

public class Account {

	//property
	private String no_rek;
	private String name;
	
	
	//function
	//void
	public void setName(){
		this.name = "aldi";
		//System.out.println("aldi");
	}
	

	public void show(){
		System.out.println("name : "+ getName());
	}
	
	//non void
	public String getName(){
		return this.name;		
	}
	
	
	public static void main(String args[]){
		Account account= new Account();
		account.setName();
		account.show();
		
		}
}
