package com.xsis.training;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Array3 {
	// set array
			public String[][] setData(int n){
				String[][] data = new String[n][n];
				//code set
				int fibo1=1;
				int fibo2=0;
				for(int i=1;i<n;i++){
					fibo1=fibo1+fibo2;
					fibo2=fibo1-fibo2;
				}
				int angka1=1;
				int angka2=0;
				
				int max = n-1;
				int nt = max/2;
				for(int y=0;y<n;y++){
					for(int x=0;x<n;x++){
						if(x==nt){
							data[x][y] = angka1 +"\t";
						}else if((y>=5)&&(x+y==max)){
							data[x][y] = fibo1 +"\t";
						}else if((y>=5)&&(x==y)){
							data[x][y] = fibo1 +"\t";
						}
					
					}
					angka1=angka1+angka2;
					angka2=angka1-angka2;
					fibo2=fibo1-fibo2;
					fibo1=fibo1-fibo2;	
				}	
				
				return data;
			}
			
			// show array
			public void showData(int n){
				System.out.println("===Menampilkan data :"+ n + "===");
				String[][] data = setData(n);
				
				for(int y=0;y<n;y++){
					for(int x=0;x<n;x++){
						if(data[x][y]==null){
							data[x][y]="\t";
						}
						System.out.print(data[x][y]);
					}
					System.out.println("\t");
				}
				
			}
			
			
			public static void main(String ags[]){
				Array2 array = new Array2();
				array.showData(9);
				//InputStreamReader isr = new InputStreamReader(System.in);
				//BufferedReader Br = new BufferedReader(isr);
				//System.out.println("Masukkan Angka");
				//String angka = Br.readLine();
				//int n = Integer.parseInt(angka);
				//System.out.print(n);
				
				//array.showData(n);
			}
}
