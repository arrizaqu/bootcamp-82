package com.xsis.training;

public class Array {
	
	// set array
	public String[][] setData(int n){
		String[][] data = new String[n][n];
		//code set
		for(int y=0;y<n;y++){
			for(int x=0;x<n;x++){
				data[x][y] = x +"."+ y +" ";
			}
		}
		
		return data;
	}
	
	// show array
	public void showData(int n){
		System.out.println("===Menampilkan data :"+ n + "===");
		String[][] data = setData(n);
		
		for(int y=0;y<n;y++){
			for(int x=0;x<n;x++){
				System.out.print(data[x][y]);
			}
			System.out.println("");
		}
		
	}
	
	
	public static void main(String ags[]){
		Array array = new Array();
		array.showData(9);
	}
}
