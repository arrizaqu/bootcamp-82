package com.xsis.day_3;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class soal_9 {
	//set array
		public String[][] setData(int n){
			String[][] data = new String[n][n];
			//code set
			int max = n-1;
			int t = max/2;
			int a = 1;
			int b = 0;
			
			int a3 = 1;
			int b3 = 0;
			for (int z=0; z<t; z++){
				a3 = a3+b3;
				b3 = a3-b3;
			}
			
			int a4 = a3;
			int b4 = b3;
			for (int y=0; y<n; y++){
				int a2 = 1;
				int b2 = 0;
				int a5 = a3;
				int b5 = b3;
				for (int x=0; x<n; x++){	
					if (y % 2 == 0 && x>y && x<=n-y-1){// segitiga atas
						data[x][y]=a +" ";
					}
					if (y % 2 == 0 && x==y && x<=t){//kiri atas ke kanan bawah
						data[x][y]=a +" ";
					}
					if (x % 2 == 0 && x<y && x<=n-y-1){//segitiga kiri
						data[x][y]=a2 +" ";
					}
					if (y % 2 == 0 && x<y && x>n-y-1){//segitiga bawah
						data[x][y]=a4 +" ";
					}
					if (x % 2 == 0 && x>y && x > n-y-1){//segitiga kanan
						data[x][y]=a5 +" ";
					}
					if (y % 2 == 0 && x==y && x>t){//kiri atas ke kanan bawah2
						data[x][y]=a5 +" ";
					}
					if (x % 2 == 0){
						a2=a2+b2;
						b2=a2-b2;
						b5=a5-b5;
						a5=a5-b5;
					}
				}
				if (y % 2 == 0){
					a=a+b;
					b=a-b;	
					b4=a4-b4;
					a4=a4-b4;
				}
				
			}
		
			
			return data;
		}
		
		//show array
		public void showData(int n){
			System.out.println("===Menampilkan data : "+ n + " ===");
			String[][] data = setData(n);
			
			for ( int y = 0; y < n; y++){
				for (int x = 0; x < n; x++){
					if(data[x][y] == null){
						data[x][y]= "  ";
					}
					System.out.print(data[x][y] + "\t");
				}
				System.out.println();
			}
		}
		
		public static void main(String args[]) throws Exception{
			InputStreamReader isr = new InputStreamReader(System.in);
			BufferedReader Br = new BufferedReader(isr);
			System.out.println("Masukkan angka : ");
			String angka = Br.readLine();
			int n = Integer.parseInt(angka);
			soal_9 Soal_9 = new soal_9();
			Soal_9.showData(n);
		}

}
