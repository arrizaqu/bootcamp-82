package com.xsis.day_3;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class soal_8 {
	//set array
		public String[][] setData(int n){
			String[][] data = new String[n][n];
			//code set
			int max = n-1;
			int t = max/2;
			int a = n;
			
			for (int y=0; y<n; y++){
				int b = n;
				for (int x=0; x<n; x++){
					if (x>y && x<=n-y-1 && y % 2 == 0){
						data[x][y]=y+1 +" ";
					}
					else if (x<y && x<=n-y-1 && x % 2 == 0 || x==y && y <= t && x % 2 == 0){
						data[x][y]=x+1 +" ";
					}
					else if (x<y && x>=n-y-1 && a % 2 == 1  || x==y && y >= t && a % 2 == 1){
						data[x][y]=a +" ";
					}
					else if (x>y && x>=n-y-1 && b % 2 == 1){
						data[x][y]=b +" ";
					}
					b=b-1;
					
				}
				a=a-1;
				
			}
			
			return data;
		}
		
		//show array
		public void showData(int n){
			System.out.println("===Menampilkan data : "+ n + " ===");
			String[][] data = setData(n);
			
			for ( int y = 0; y < n; y++){
				for (int x = 0; x < n; x++){
					if(data[x][y] == null){
						data[x][y]= "  ";
					}
					System.out.print(data[x][y] + "\t");
				}
				System.out.println();
			}
		}
		
		public static void main(String args[]) throws Exception{
			InputStreamReader isr = new InputStreamReader(System.in);
			BufferedReader Br = new BufferedReader(isr);
			System.out.println("Masukkan angka : ");
			String angka = Br.readLine();
			int n = Integer.parseInt(angka);
			soal_8 Soal_8 = new soal_8();
			Soal_8.showData(n);
		}

}
