package com.xsis.day_3;

import java.util.Scanner;

public class soal_5 {
    public static int hitungFPB(int x, int y) {
        if (x > y) { //memeriksa apakah x lebih besar dari y
            if (x % y == 0) { //jika sisa bagi x dan y adalah 0
                return y; //maka FPBnya adalah y
            } else { //jika sisa bagi x dan y tidak sama dengan 0
                return hitungFPB(y, x % y); //fungsi rekursif yang memanggil fungsi itu sendiri
            }
        } else { //jika x tidak lebih besar dari y. atau x &lt; y
            if (y % x == 0) { //jika sisa bagi y dan x adalah 0
                return x; //maka FPBnya adalah x
            } else { //jika sisa bagi y dan x tidak sama dengan 0
                return hitungFPB(x, y % x); //fungsi rekursif yang memanggil fungsi itu sendiri
            }
        }
    }
 
    public static void main(String[] args) {
        System.out.println(hitungFPB(6, 12)); //implementasi penghitungan FPB dari 228 dan 90
    }
}