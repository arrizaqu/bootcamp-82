package com.xsis.day_3;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import template.temp;

public class post_test {
	//set array
		public String[][] setData(int n){
			String[][] data = new String[n*2-1][n];
			//code set
			int max = n-1;
			int t = max/2;
			for (int y=0; y < n; y++){
				int a=1;
				int b=0;
				for (int x=0; x<n*2-1; x++){
					if (x==y || x==n-y-1){
						data[x][y]=a+" ";
					}
					if (x+y==max*2 || x-y == max){
						data[x][y]=a+" ";
					}
					if (x>=max){
						b=a-b;
						a=a-b;
					} else if(x<max){
						a=a+b;
						b=a-b;
						
					}
														
				}
			}
			return data;
		}
		
		//show array
		public void showData(int n){
			System.out.println("===Menampilkan data : "+ n + " ===");
			String[][] data = setData(n);
				
			for ( int y = 0; y < n; y++){
				for (int x = 0; x < n*2-1; x++){
					if(data[x][y] == null){
						data[x][y]= "  ";
					}
					System.out.print(data[x][y] + "\t");
				}
				System.out.println();
			}
		}
				
				
		public static void main(String args[]) throws Exception{
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader Br = new BufferedReader(isr);
		System.out.println("Masukkan angka : ");
		String angka = Br.readLine();
		int n = Integer.parseInt(angka);
		post_test Post = new post_test();
		Post.showData(n);
		}


}
