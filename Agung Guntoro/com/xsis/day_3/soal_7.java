package com.xsis.day_3;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class soal_7 {
	//set array
		public String[][] setData(int n){
			String[][] data = new String[n][n];
			//code set
			int max = n-1;
			int t = max/2;
			int a = 1;
			int b = 0;
			int ax = 1;
			int bx = 0;
			
			
			for (int z=1; z<n; z++){
				ax=ax+bx;
				bx=ax-bx;
								
			}
			for (int y=0; y<n; y++){
				int a2 = 1;
				int b2 = 0;
				int ay = 1;
				int by = 0;
				for (int w=1; w<n; w++){
					ay=ay+by;
					by=ay-by;
									
				}
				for (int x=0; x<n; x++){
					if (x > y && x <= n-y-1){
						data[x][y]= a + " ";
					}
					else if (x == y && x <= t){
						data[x][y]= a + " ";
					}
					else if (x == y && x >= t){
						data[x][y]= ax + " ";
					}
					else if (x < y && x < n-y-1){
						data[x][y]= a2 + " ";
					}
					else if (x < y && x >= n-y-1){
						data[x][y]= ax + " ";
					}
					else if (x > y && x >= n-y-1){
						data[x][y]= ay + " ";
					}
										
					
					a2=a2+b2;
					b2=a2-b2;
					by=ay-by;
		    		ay=ay-by;
					
					
				}
				a=a+b;
				b=a-b;
				bx=ax-bx;
	    		ax=ax-bx;
	    		
	    		
				
				
			}
			
			
			return data;
		}
		
		//show array
		public void showData(int n){
			System.out.println("===Menampilkan data : "+ n + " ===");
			String[][] data = setData(n);
			
			for ( int y = 0; y < n; y++){
				for (int x = 0; x < n; x++){
					if(data[x][y] == null){
						data[x][y]= "  ";
					}
					System.out.print(data[x][y] + "\t");
				}
				System.out.println();
			}
		}
		
		public static void main(String args[]) throws Exception{
			InputStreamReader isr = new InputStreamReader(System.in);
			BufferedReader Br = new BufferedReader(isr);
			System.out.println("Masukkan angka : ");
			String angka = Br.readLine();
			int n = Integer.parseInt(angka);
			soal_7 Soal_7 = new soal_7();
			Soal_7.showData(n);
		}

}
