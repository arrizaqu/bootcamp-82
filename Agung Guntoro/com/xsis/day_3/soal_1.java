package com.xsis.day_3;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class soal_1 {
	
	//set array
	public String[][] setData(int n){
		String[][] data = new String[n][n];
		//code set
		
		for ( int y = 0; y < n; y++){
			int a = 1;
			int b = 0;
			for (int x = 0; x < n; x++){
				if(y==n-n){
					data[x][y]= a +" ";
				}
				a=a+b;
				b=a-b;
			}
		}
		return data;
	}
	
	//show array
	public void showData(int n){
		System.out.println("===Menampilkan data : "+ n + " ===");
		String[][] data = setData(n);
		
		for ( int y = 0; y < n; y++){
			for (int x = 0; x < n; x++){
				if(data[x][y] == null){
					data[x][y]= "    ";
				}
				System.out.print(data[x][y]);
			}
			System.out.println();
		}
	}
	
	public static void main(String args[]) throws Exception{
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader Br = new BufferedReader(isr);
		System.out.println("Masukkan angka : ");
		String angka = Br.readLine();
		int n = Integer.parseInt(angka);
		soal_1 Soal_1 = new soal_1();
		Soal_1.showData(n);
	}

}
