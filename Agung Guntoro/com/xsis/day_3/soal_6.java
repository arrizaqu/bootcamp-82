package com.xsis.day_3;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class soal_6 {
	
	//set array
	public String[][] setData(int n){
		String[][] data = new String[n][n];
		//code set
		
		for ( int y = 0; y < n; y++){
			int a = 1;
			int b = 0;
			for (int x = 0; x < n; x++){
				if(x==y || x+y==n-1){ //untuk silang dua2nya
					data[x][y]= a +" ";
				}
				else if(x>y && x+y<=n-1){ // tampil A
					data[x][y]= "A";
				}
				else if(x>y && x+y>=n-1){ // tampil B
					data[x][y]= "B";
				}
				else if(x<y && x+y>=n-1){ // tampil C
					data[x][y]= "C";
				}
				else if(x<y && x+y<=n-1){ // tampil D
					data[x][y]= "D";
				}
				a=a+b;
				b=a-b;
			}
		}
		return data;
	}
	
	//show array
	public void showData(int n){
		System.out.println("===Menampilkan data : "+ n + " ===");
		String[][] data = setData(n);
		
		for ( int y = 0; y < n; y++){
			for (int x = 0; x < n; x++){
				if(data[x][y] == null){
					data[x][y]= "  ";
				}
				System.out.print(data[x][y] + "\t");
			}
			System.out.println();
		}
	}
	
	public static void main(String args[]) throws Exception{
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader Br = new BufferedReader(isr);
		System.out.println("Masukkan angka : ");
		String angka = Br.readLine();
		int n = Integer.parseInt(angka);
		soal_6 Soal_6 = new soal_6();
		Soal_6.showData(n);
	}

}
