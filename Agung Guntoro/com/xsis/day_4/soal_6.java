package com.xsis.day_4;

import java.io.BufferedReader;
import java.io.InputStreamReader;


public class soal_6 {
	public char[] getChar(){
		char[] data2 = new char[26];
		for(char y = 'A';y<='Z';y++){
			data2[y-'A']=y;
		}
		return data2;
	}
	
	public int[] getAngka(int n){
		int[] dt = new int[n*2-1];
		
		for (int y =0; y<(n*2)-1; y++){
			if (y==0)
				dt[y]=1;
			else
				dt[y]=dt[y-1]+2;
		}
		return dt;
	}
	//set array
		public String[][] setData(int n){
			String[][] data = new String[n*2-1][n];
			char[] data2 = getChar();
			//int[] dt = getAngka(n);
			int[] angka = getAngka(n);
			//code set
			int max = n-1;
			int t = max/2;
			int spasi = 0;
			
			
			for (int y=0; y < n; y++){
				int a = 0;
				for (int x=0; x<n*2-1; x++){
					if (x+y==max || x+y>max && x-y <= max){
						data[x][y] = angka[a]+" ";
						if (x<max)
						a++;	
						else a--;
					}
					
				}
				
			}
			return data;
		}
		
		//show array
		public void showData(int n){
			System.out.println("===Menampilkan data : "+ n + " ===");
			String[][] data = setData(n);
			char[] data2 = getChar();
			int abjad = 0;
				
			for ( int y = 0; y < n; y++){
				for (int x = 0; x < n*2-1; x++){
					if(data[x][y] == null){
						data[x][y]="  ";
					} 
					System.out.print(data[x][y] + "\t");
				}
				System.out.println();
			}
		}
				
				
		public static void main(String args[]) throws Exception{
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader Br = new BufferedReader(isr);
		System.out.println("Masukkan angka : ");
		String angka = Br.readLine();
		int n = Integer.parseInt(angka);
		soal_6 Soal_6 = new soal_6();
		Soal_6.showData(n);
		}


}
