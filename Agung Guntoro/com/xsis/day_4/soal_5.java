package com.xsis.day_4;

import java.io.BufferedReader;
import java.io.InputStreamReader;


public class soal_5 {
	public char[] getChar(){
		char[] data2 = new char[26];
		for(char y = 'A';y<='Z';y++){
			data2[y-'A']=y;
		}
		return data2;
	}
	//set array
		public String[][] setData(int n){
			String[][] data = new String[n][n];
			char[] data2 = getChar();
			//code set
			int max = n-1;
			int t = max/2;
			int spasi = 0;
			int a = 0-t+1;
			
			for (int y=0; y < n; y++){
				for (int x=0; x<n; x++){
					if (x-y<=t && x+y >=t && y-x<=t && y+x<n+t)
					data[x][y] = x+a+" ";
				}
				if (y<t){
					a+=1;
				}else a-=1;
			}
			return data;
		}
		
		//show array
		public void showData(int n){
			System.out.println("===Menampilkan data : "+ n + " ===");
			String[][] data = setData(n);
			char[] data2 = getChar();
			int abjad = 0;
				
			for ( int y = 0; y < n; y++){
				for (int x = 0; x < n; x++){
					if(data[x][y] == null){
						data[x][y]="  ";
					} 
					System.out.print(data[x][y] + "\t");
				}
				System.out.println();
			}
		}
				
				
		public static void main(String args[]) throws Exception{
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader Br = new BufferedReader(isr);
		System.out.println("Masukkan angka : ");
		String angka = Br.readLine();
		int n = Integer.parseInt(angka);
		soal_5 Soal_5 = new soal_5();
		Soal_5.showData(n);
		}


}
