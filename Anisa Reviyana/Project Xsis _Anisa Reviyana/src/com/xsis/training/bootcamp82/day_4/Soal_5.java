package com.xsis.training.bootcamp82.day_4;

import java.util.Scanner;

public class Soal_5 {
	private static Scanner scan;
	public String[][] setData(int n){
        String[][] data = new String[n][n];
        int max=n-1;
        int tengah=max/2;
        
        for(int y = 0; y < n; y++){
        	int nil=1;
            for(int x= 0; x < n; x++)
            { if((x<=y+tengah)&&(x+y<=max+tengah)&&(x+y>=tengah)&&(y-x<=tengah)){//box 2
        		data[x][y] = nil+" ";
        		nil++;
        	}
            
            }
        }
        return data;
 }
 public void showData(int n){
        String[][] data = setData((n*2)-1);

        for(int y = 0; y < n*2-1 ; y++){
            for(int x= 0; x < n*2-1; x++){
                if(data[x][y] == null){
                    data[x][y] = "  ";
                }
                
                System.out.print(data[x][y]);
            }
            
            System.out.println(" ");
        }
        
    }
		public static void main(String args[]){
		Soal_5  logic= new Soal_5();
		scan = new Scanner(System.in);
		System.out.print("Masukkan data: ");
		int n=scan.nextInt();
		logic.showData(n);
		
	}
}