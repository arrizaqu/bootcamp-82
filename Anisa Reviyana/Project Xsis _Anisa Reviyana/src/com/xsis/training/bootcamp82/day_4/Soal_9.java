package com.xsis.training.bootcamp82.day_4;

import java.util.Scanner;

public class Soal_9 {
	private static Scanner scan;
	public String[][] setData(int n){
        String[][] data = new String[4*n-1][n*4];
        int max=n-1;
        int tengah=max/2;
        int full=max*2;
        
        for(int y = 0; y < n *2 ; y++){
        	int nil=1;
            for(int x= 0; x < n*2-1; x++)
            { if((x<=y+(tengah/2))&&(x+y<=tengah+(tengah/2))&&(x+y>=tengah/2)&&(y-x<=tengah/2)){//box 2
        		data[x][y] = nil+" ";
        		if(x>=tengah){
        			nil--;
        		}else{
        		nil++;
        		}
        	}else{
        		if(((x<=y+tengah)&&(x+y<=max+tengah) && ((x+y>=tengah)&& (y-x<=tengah)))){//box 2
            		data[x+(n-1)][y] = nil+" ";
            		if(x>=tengah){
            			nil--;
            		}else{
            		nil++;
            		}
        	}
            
            }
        }
        }
        return data;
 }
 public void showData(int n){
        String[][] data = setData(((n*2)-1)*2);

        for(int y = 0; y < n*2-1 ; y++){
            for(int x= 0; x < (n*2-1)*2; x++){
                if(data[x][y] == null){
                    data[x][y] = "  ";
                }
                
                System.out.print(data[x][y]);
            }
            
            System.out.println(" ");
        }
        
    }
		public static void main(String args[]){
		Soal_9  logic= new Soal_9();
		scan = new Scanner(System.in);
		System.out.print("Masukkan data: ");
		int n=scan.nextInt();
		logic.showData(n);
		
	}
}