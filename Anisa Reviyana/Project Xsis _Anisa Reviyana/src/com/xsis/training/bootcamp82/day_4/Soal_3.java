package com.xsis.training.bootcamp82.day_4;

import java.util.Scanner;

public class Soal_3 {
	private static Scanner scan;
	
	public String[][] setData(int n){
        String[][] data = new String[n][n];
        int max=n-1;
        int tengah=max/2;
        for(int y = 0; y < n; y++){
        	int nil=1;
            for(int x= 0; x < n; x++)
            { 
            	if((x+y>=max/2)&&(x-y<=tengah)){
            		data[x][y] = nil+"\t";
            		nil++;
            	}	
            }
        }
        return data;
 }
 public void showData(int n){
        String[][] data = setData((n*2)-1);

        for(int y = 0; y < n; y++){
            for(int x= 0; x < n*2-1; x++){
                if(data[x][y] == null){
                    data[x][y] = "\t";
                }
                
                System.out.print(data[x][y]);
            }
            
            System.out.println("\t");
        }
        
    }
		public static void main(String args[]){
		Soal_3  logic= new Soal_3();
		scan = new Scanner(System.in);
		System.out.print("Masukkan data: ");
		int n=scan.nextInt();
		logic.showData(n);
		
	}
}