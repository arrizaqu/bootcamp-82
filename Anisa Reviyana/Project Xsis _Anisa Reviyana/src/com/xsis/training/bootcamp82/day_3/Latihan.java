package com.xsis.training.bootcamp82.day_3;
import java.util.Scanner;

public class Latihan {
	//set array
		public String[][] setData(int n){
			String[][] data=new String[n][n];
					int max=n-1;
					int nt = max/2;
			// code set
			for(int y=0; y<n; y++){
				for(int x=0;x<n;x++){
					if(x==y){
						data[x][y]= x+"."+y+" ";
					}
					if(x+y==max){
						data[x][y]= x+"."+y+" ";
					}
					if(x==nt){
						data[x][y]= x+"."+y+" ";
				}
					if(y==nt){
						data[x][y]= x+"."+y+" ";
				}
					if(x==0){
						data[x][y]= x+"."+y+" ";
					}
					if(y==n-1){
						data[x][y]= x+"."+y+" ";
					}
					if(x==n-1){
						data[x][y]= x+"."+y+" ";
					}
					if(y==0){
						data[x][y]= x+"."+y+" ";
					}
				}
			}
			return data;
		}
		//show array
		public void showData(int n){
			System.out.println("=== Menampilkan data : "+n+"===");
			String[][] data = setData(n);
			
			for(int y=0; y<n; y++){
				for(int x=0;x<n;x++)
				{
					if(data[x][y]==null){
						data[x][y]="    ";
					}
					System.out.print(data[x][y]+ " ");
				}
				System.out.println("");
			}
		}
		public static void main(String args[]){
		Latihan  logic= new Latihan ();
		Scanner scan =new Scanner(System.in);
		System.out.print("Masukkan data: ");
		int n=scan.nextInt();
		logic.showData(n);
		
	}
	}