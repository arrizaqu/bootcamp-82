package com.xsis.training.bootcamp82.day_3;

import java.util.Scanner;

public class Soal_8 {
	//set array
			public String[][] setData(int n){
				String[][] data=new String[n][n];
				int max=n-1;
				int nt = max/2;
				int a=1;
				int b=n;
		// code set
		for(int y=0; y<n; y++){
			int c=1;
			int d=n;
			for(int x=0;x<n;x++){
				if((x+y<=n-1)&&(x>=y)&&(y%2==0)){
					data[x][y]= a+"";
				}else if((x+y<=n-1)&&(x<=y)&&(x%2==0)){
					data[x][y]= c+"";
				}else if((x+y>=n-1)&&(x<=y)&&(y%2==0)){
					data[x][y]= b+"";
			}else if((x+y>=n-1)&&(x>=y)&&(x%2==0)){
				data[x][y]= d+"";
			}
				c++;
				d--;
			}
			a++;
			b--;
		}
		return data;
	}
	//show array
	public void showData(int n){

		String[][] data = setData(n);
		
		for(int y=0; y<n; y++){
			for(int x=0;x<n;x++)
			{
				if(data[x][y]==null){
					data[x][y]=" ";
				}
				System.out.print(data[x][y]+ " ");
			}
			System.out.println("");
		}
	}
	public static void main(String args[]){
	Soal_8   logic= new Soal_8  ();
	Scanner scan =new Scanner(System.in);
	System.out.print("Masukkan data: ");
	int n=scan.nextInt();
	logic.showData(n);

	}
}