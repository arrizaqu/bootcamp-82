package com.xsis.training.bootcamp82.day_3;

import java.util.Scanner;

public class PostTest {
	
	private static Scanner scan;
	public int[] getFib(int n){
		 
        int[] data = new int[n];
       // int loop=1;
  
        for(int y = 0; y < n; y++){
          
        	 if(y<2){
        		 data[y] = 1;
        		 }else{
        		 data[y]= data[y-1]+data[y-2];
        	 }
        	 //loop++;
        }
        
        return data;
    }
	 public String[][] setData(int n){
	        String[][] data = new String[n][n];
	        int[] fib1 = getFib(n);
	        int max=n-1;
	        int max2= max/2;
	        for(int y = 0; y < n; y++){
	            for(int x= 0; x < n; x++){
	                
	               if( y + x ==max ) {
	                       
	                                data[x][y]=  fib1[y] + "\t";
	                        
	                }
	                else if(y==x) 
	                {
	                       
	                                data[x][y]= fib1[y]+"\t";
	                       
	                }
	                else if(y+x==max2) 
	                {
	                      
	                                data[x][y]= fib1[x]+"\t";
	                      
	                }
	                else if( x-y==max2)
	                {
	                      
	                                data[x][y]= fib1[n-x-1]+"\t";
	                      
	                }
	               
	                
	            }
	        }
	        
	        
	        return data;
	 }
	 public void showData(int n){
	        String[][] data = setData((n*2)-1);

	        for(int y = 0; y < n; y++){
	            for(int x= 0; x < n*2-1; x++){
	                if(data[x][y] == null){
	                    data[x][y] = "\t";
	                }
	                
	                System.out.print(data[x][y]);
	            }
	            
	            System.out.println("\t");
	        }
	        
	    }
			public static void main(String args[]){
			PostTest  logic= new PostTest ();
			scan = new Scanner(System.in);
			System.out.print("Masukkan data: ");
			int n=scan.nextInt();
			logic.showData(n);
			
		}
}