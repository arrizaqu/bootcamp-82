package com.xsis.training.bootcamp82.day_3;

import java.util.Scanner;

public class Soal_6 {
	//set array
		public String[][] setData(int n){
			String[][] data=new String[n][n];
			// code set
			int max = n-1;
			int nt = max/2;
			
			
			for(int y=0; y<n; y++){
				int a=1;
				int b=0;
				
				for(int x=0;x<n;x++){
					if((y<x)&&(x<n-y-1)){
					data[x][y]= "A\t";
					}
					else if(y+x==max){
						data[x][y]= a+"\t";
						}
					else if((y>x)&&(x>n-y-1)){
					data[x][y]= "C\t";
					}
					else if(y<x){
					data[x][y]= "B\t";
					}else if(y==x){
					data[x][y]= a+"\t";
					}
					a=a+b;
					b=a-b;
				}
				
			}
			return data;
		}
		//show array
		public void showData(int n){
			
			String[][] data = setData(n);
			
			for(int y=0; y<n; y++){
				for(int x=0;x<n;x++){
					if(data[x][y]==null){
						data[x][y]="D\t";
					}
					System.out.print(data[x][y]);
				}
				System.out.println("");
			}
		}
		public static void main(String args[]){
		Soal_6 logic= new Soal_6();
		Scanner scan =new Scanner(System.in);
		System.out.print("Masukkan data: ");
		int n=scan.nextInt();
		logic.showData(n);
	}
	}
