package com.xsis.d3;

public class LogicArray6 {
	
	// set Array
	public String[][]  setData (int n){
		String[][] data = new String [n][n];
			int max = n-1;
			int nt = max/2;
			int mulai = nt % 2;
			int a=0;
			int b=1;
		
		//code set 
			
		for (int y = 0; y < n; y++){ 
			int c=0;
			int d=1;
			for (int x = 0; x < n; x++){ 
				if (y==x) {
					data[x][y] = b + " ";
					b=a+b;
					a=b-a;
				}	
				else if (y+x==max)
				{
					data[x][y] = d + " ";
				} 
				else if ((x+y<max) && (x>y)) {
					data[x][y] = "A";
				} 
				else if ((x+y>max) && (x>y)) {
					data[x][y] = "B";
				}
				else if ((x+y>max) && (x<y)) {
					data[x][y] = "C";
				}
				else if ((x+y<max) && (x<y)) {
					data[x][y] = "D";
				}
				d=c+d;
				c=d-c;
			}	
		}
		
		return data;
	
	}
	
	//show array
	public void showData(int n){
		System.out.println("=== Menampilkan data : " + n + " ===");
		String [][] data = setData(n);
		
		//x=baris
		//y=kolom
		
		for (int y = 0; y < n; y++){ 
			for (int x = 0; x < n; x++){ 
				if(data[x][y] == null){
					data[x][y] = "    ";
				}
			System.out.print(data [x][y] +"\t");
			}
		System.out.println("");
		}
	}
	
	public static void main(String args[]){
		LogicArray6 logic = new LogicArray6 ();
		int n = 9;
		
		logic.showData(n);
	}
}

