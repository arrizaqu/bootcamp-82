package com.xsis.d3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class LogicArray8a {
	
	//set array
	public String[][] setData(int n){
		String[][] data = new String[n][n];
		
		//set code

		int p=0;
		int q=1;
		for(int i=0; i<n; i++){
			q=p+q;
			p=q-p;
		}
		int m=q;
		int l=p;
		
		int c=0;
		int d=1;
		
		for(int y=0; y<n; y++){
			int a=0;
			int b=1;
			int e=q;
			int f=p;
			for(int x=0; x<n; x++)	
			{		
				if(x+y>=n-1 && x>=y){
					if (x%2==0) {
						data[x][y]=f + "\t";
					}
				}
				else if(x+y>=n-1 && x<=y ){
					if (y%2==0) {
						data[x][y]=l + "\t";
					}
				}
				else if(x+y<=n-1 && x<=y){
					if (x%2==0) {
						data[x][y]=b + "\t";
					}
				}
				else if(x+y<=n-1 && x>=y){
					if (y%2==0) {
						data[x][y]=d + "\t";
					}
				}
				b=a+b;
				a=b+a;
				f=e-f;
				e=e-f;
			
			}
			l=m-l;
			m=m-l;
			d=c+d;
			c=d-c;
		}
		
		
		return data;
		
	}
	
	//show array
	public void showData(int n){
		System.out.print("Menampilkan fibonaci \n");
		String[][] data = setData(n);
		
		for(int x=0; x<n; x++){
			int index=0;
			for(int y=0; y<n; y++){
				for(x=0; x<n; x++){
					if(data[x][y]==null){
						data[x][y]="\t";
					}
					System.out.print(data[x][y] + "\t");
				}
				System.out.println();				
			}		
			
			System.out.println();
		}
	}
	
	public static void main(String[] args) throws IOException{
		LogicArray8a logic = new LogicArray8a();
		logic.showData(9);
		
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		//System.out.print("Masukkan Angka fibonaci = ");
		String angka = br.readLine();
		int n = Integer.parseInt(angka);
		logic.showData(n);
		
	}
}
