package com.xsis.d3;

public class LogicArray8 {
	
	public int[] getFib(int n){					//menampilkan pola fibonanci mirror
        
        int[] data = new int[n];
        int loop = 1;
        for(int y = 0; y < n; y++){
           
            if(y < 2){ 
                data[y] = 1;
            } else {
                data[y] = data[y - 1] + data[y - 2];
            }
            
            loop++;
        }
        
        return data;
    }
    
    public String[][] setData(int n){
        String[][] data = new String[n][n];
        int[] fib1 = getFib(n);
        
        int max = n - 1;
        
        for(int y = 0; y < n; y++){
        	
            for(int x= 0; x < n; x++){
                if( y + x >= max && x <= y) {
                        if (y%2==0){
                            data[x][y]= fib1[n-y-1] + "\t";
                        }
                }        
                else if(y+x<=max && x>=y) {							//atas
                        if (y%2==0){
                            data[x][y]= fib1[y]+ "\t";
                        }
                }
                else if(y+x<=max && x<=y) {							//Kiri
                       if (x%2==0){
                           data[x][y]= fib1[x]+ "\t";
                       }
                }
                else if(y+x>=max && x>=y)  {						//Kanan
                		if (x%2==0){
                        data[x][y]= fib1[n-x-1] + "\t ";
                		}
                } 
            }
        }
        
        return data;
    }
    
    public void showData(int n){
        String[][] data = setData(n);
        for(int y = 0; y < n; y++){
            for(int x= 0; x < n; x++){
                if(data[x][y] == null){
                    data[x][y] = "\t";
                }
                
                System.out.print(data[x][y] +"\t" );
            }
            
            System.out.println("");
        }
        
    }
    
    public static void main(String[] args) {
         LogicArray8 jawab = new LogicArray8();
         jawab.showData(9);
    }
}
