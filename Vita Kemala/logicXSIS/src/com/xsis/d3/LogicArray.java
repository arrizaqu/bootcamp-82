package com.xsis.d3;

	public class LogicArray {
		
		// set Array
		public String[][]  setData (int n){
			String[][] data = new String [n][n];
				int max = n-1;
				int nt = max/2;
				int mulai = nt % 2;
				int a=0;
				int b=1;
			
			//code set 
			for (int y = 0; y < n; y++){ 
				for (int x = 0; x < n; x++){ 
					if (y==mulai) {
						data[x][y] = b + " ";
						b=a+b;    						//deret fibonanci
						a=b-a;
					}	
				}
			}
			return data;
		
		}
		
		//show array
		public void showData(int n){
			System.out.println("=== Menampilkan data : " + n + " ===");
			String [][] data = setData(n);
			
			//x=baris
			//y=kolom
			
			for (int y = 0; y < n; y++){ 
				for (int x = 0; x < n; x++){ 
					if(data[x][y] == null){
						data[x][y] = "    ";
					}
				System.out.print(data [x][y]);
				}
			System.out.println("");
			}
		}
		
		public static void main(String args[]){
			LogicArray logic = new LogicArray ();
			int n = 9;
			
			logic.showData(n);
		}
}


