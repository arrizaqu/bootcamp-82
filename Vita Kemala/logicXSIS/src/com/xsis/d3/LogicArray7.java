package com.xsis.d3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class LogicArray7 {
	
	//set array
	public String[][] setData(int n){
		String[][] data = new String[n][n];
		
		//set code

		int p=0;
		int q=1;
		for(int i=0; i<n; i++){
			q=p+q;
			p=q-p;
		}
		int m=q;
		int l=p;
		
		int c=0;
		int d=1;
		
		for(int y=0; y<n; y++){
			int a=0;
			int b=1;
			int e=q;
			int f=p;
			for(int x=0; x<n; x++)	
			{		
				if(x+y>=n-1 && x>=y){
					data[x][y]=f + "\t";
				}
				else if(x+y>=n-1 && x<=y ){
					
					data[x][y]=l + "\t";
				}
				else if(x+y<=n-1 && x<=y){
					
					data[x][y]=b + "\t";
				}
				else if(x+y<=n-1 && x>=y){
					
					data[x][y]=d + "\t";
				}
				b=a+b;
				a=b+a;
				f=e-f;
				e=e-f;
			
			}
			l=m-l;
			m=m-l;
			d=c+d;
			c=d-c;
		}
		
		
		return data;
		
	}
	
	//show array
	public void showData(int n){
		System.out.print("Menampilkan fibonaci \n");
		String[][] data = setData(n);
		
		for(int x=0; x<n; x++){
			int index=0;
			for(int y=0; y<n; y++){
				for(x=0; x<n; x++){
					if(data[x][y]==null){
						data[x][y]=" ";
					}
					System.out.print(data[x][y]);
				}
				System.out.println();
				
			   /* if(x==0 || y==0 ||x==1 || y==1 || x==8 || x==7  )		
				System.out.print(1 +"  ");
				if(y==3 && x>1 && x<7)
					System.out.print(2 +"  ");
				if(y==4 && x>1 && x<7)
					System.out.print(2 +"  ");
				if(y==7 && x>1 && x<7)
					System.out.print(1 +"  ");
				if(y==8 && x>1 && x<7)
					System.out.print(1 +"  ");
				if(x==2 && y>1 &&y<5)
					System.out.print(2 +"  ");
				if(x==3 && y>1 &&y<5)
					System.out.print(2 +"  ");
				if(x==5 && y>1 &&y<5)
					System.out.print(2 +"  ");
				if(x==6 && y>1 &&y<5)
					System.out.print(2 +"  ");
				if(x==4 && y==4)
					System.out.print(3 +"  ");
				if(x==4 && y==5)
					System.out.print(2 +"  ");
				if(x==4 && y==6)
					System.out.print(2 +"  ");
				
				if(x>y && x<n-y-1)		
					System.out.print(data[x] + " ");
				if(y>x && x==y)		
					System.out.print(data[x] + " ");
				if(x<nt)
					index++;
				else
					index--;*/
				
			}		
			
			System.out.println();
		}
	}
	
	public static void main(String[] args) throws IOException{
		LogicArray7 logic = new LogicArray7();
		logic.showData(7);
		
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		//System.out.print("Masukkan Angka fibonaci = ");
		String angka = br.readLine();
		int n = Integer.parseInt(angka);
		logic.showData(n);
		
	}
}
