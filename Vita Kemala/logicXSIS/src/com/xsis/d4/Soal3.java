package com.xsis.d4;


public class Soal3{    
	
	// set Array
	public String[][]  setData (int n){
		String[][] data = new String [n][n];
			int max = n-1;
			int nt = max/2;
	
		
		//code set 
		for (int y = 0; y < n; y++){ 
			int a=1;
			for (int x = 0; x < n; x++){ 
					if ((x+y>=max) && (x>=nt)) {
						data[x][y] = a + " ";
						a++;
					}
			}
		}
		return data;
	
	}
	
	//show array
	public void showData(int n){
		System.out.println("=== Menampilkan data : " + n + " ===");
		String [][] data = setData(n*2-1);
		
		//x=baris
		//y=kolom
		
		for (int y = 0; y < n; y++){ 
			for (int x = 0; x < n*2-1; x++){ 
				if(data[x][y] == null){
					data[x][y] = " ";
				}
			System.out.print(data [x][y] +" ") ;
			}
		System.out.println(" ");
		}
	}
	
	public static void main(String args[]){
		Soal3 soal3 = new Soal3 ();
		int n = 9;
		
		soal3.showData(n);
	}
}
