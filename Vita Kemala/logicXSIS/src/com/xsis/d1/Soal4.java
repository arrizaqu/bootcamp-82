package com.xsis.d1;

public class Soal4 {

	public static void main(String[] args) {
		Soal4 soal4 = new Soal4();
		soal4.deret_asterik();
	}
	
	public void deret_asterik(){
		
		for (int baris = 1; baris <= 9; baris++)
        {
            for (int kolom = 1; kolom <= 9; kolom++)
            {
                if (baris + kolom == 10) { System.out.print("* "); }
                else if (baris == kolom) { System.out.print("* "); }
                else if (baris == 5 || kolom == 5) { System.out.print("* "); }
                else { System.out.print("  "); }
            }
            System.out.println();
        }
		
	}

}
