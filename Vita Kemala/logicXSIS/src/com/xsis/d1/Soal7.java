package com.xsis.d1;

public class Soal7 {

	public static void main(String[] args) {
		Soal7 soal7 = new Soal7();
		soal7.pasir();

	}
	
	public void pasir(){
		for (int baris = 1; baris <= 9; baris++)
        {
            for (int kolom = 1; kolom <= 9; kolom++)
            {
                if (baris <= 5) 
                {
                    if ((kolom >= baris) && (kolom <= (9 - (baris - 1)))) { System.out.print("* "); }
                    else { System.out.print("  "); }
                }
                else 
                {
                    if ((kolom <= baris ) && (kolom >= (9-(baris-1)))) { System.out.print("* "); }
                    else { System.out.print("  "); }
                }
            }
            System.out.println();
        }
	}

}