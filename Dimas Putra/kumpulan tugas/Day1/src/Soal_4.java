
public class Soal_4 {
	
	public static void main(String[] args){
		Soal_4 soal_1 = new Soal_4();
		soal_1.deretBintang();
	}
	
	public void deretBintang(){
		int n=9;
		int tengah=n/2;
		for(int i=1;i<=n;i++){
			for(int j=1;j<=n;j++){
				if(i<=tengah){
					if(j==n/2+1||j==(n-i)+1||j==i){
						System.out.print("*");
					}else System.out.print(" ");
				}
				if(i==tengah+1){
					System.out.print("*");
				}
				if(i>tengah+1){
					if(j==(n-i)+1||j==n/2+1||j==i){
						System.out.print("*");
					}else System.out.print(" ");
				}
			}System.out.println();
		}
	}
}
