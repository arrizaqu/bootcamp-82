package com.xsis.day5;

public class Soal_10dinamis {
	
	public int[] getFib(int n){
	        
	        int[] data = new int[n*4];
	        for(int y = 0; y < n*2; y++){
	        	   if(y < 2){ 
	                   data[y] = 1;
	               } else {
	                   data[y] = data[y - 1] + data[y - 2];
	               }
	        }
	        return data;
	    }
	
	public String[][] setData(int n){
        String[][] data = new String[n*4][n*2];
        
        int[] fib1 = getFib(n);
        
        for(int y = 0; y < n*2-1; y++){
        	int g=0;
        	int j=0;
            for(int x= 0; x < n*4-3; x++){
            	
            	//data[x][y] = x+"."+y+" ";
            	
          	if(x+y>=n-1&&x-y<n&&y<n){        
            	data[x][y] = fib1[j] + " ";
            	if(x>n-2){
          		j--;
            	}else j++;
            	
            	
            	}
            	else if(x-y>n-n*2&&x+y<((n-1)*3)+1&&y>n-1){
           		data[x][y] = fib1[j] + " ";
            		if(x>n-2){
               		j--;
               	}else j++;
           		
            	}
           	if(x+y>=9&&x-y<10&&x-y>=3&&x+y<16){
           		data[x][y] = fib1[g] + " ";
        		if(x>n*2){
           		g--;
           	}else g++;
           	}
            	
            }
        }

        return data;
    }
	
	public void showData(int n){
        String[][] data = setData(n);

        for(int y = 0; y < n*2-1; y++){
            for(int x= 0; x < n*4-3; x++){
             if(data[x][y] == null){
                  data[x][y] ="  ";
                }
             System.out.print(data[x][y]+"  ");
           
            } System.out.println("");
        }
    }
	
	 public static void main(String[] args) {
         Soal_10dinamis jawab = new Soal_10dinamis();
         jawab.showData(4);
    }
}
