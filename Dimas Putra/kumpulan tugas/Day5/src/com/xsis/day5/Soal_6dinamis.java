package com.xsis.day5;

public class Soal_6dinamis {
	
	public int[] getFib(int n){
	        
	        int[] data = new int[n*2];
	        for(int y = 0; y < n*2; y++){
	        	if(y%2==0){
		               
		          }else  data[y] = y;
		  
	        }
	        return data;
	    }
	
	public String[][] setData(int n){
        String[][] data = new String[n*2][n];
        
        int[] fib1 = getFib(n);
        
        for(int y = 0; y < n; y++){
        	int j=1;
            for(int x= 0; x < n*2-1; x++){
            	if(x+y>=n-1&&x-y<n){        
            	data[x][y] = j + " ";
            	if(x>n-2){
            		j-=2;
            	}else j+=2;
            	
            	}else{}
            }
        }

        return data;
    }
	
	public void showData(int n){
        String[][] data = setData(n);

        for(int y = 0; y < n; y++){
            for(int x= 0; x < n*2-1; x++){
             if(data[x][y] == null){
                  data[x][y] ="  ";
                }
             System.out.print(data[x][y]+"  ");
           
            } System.out.println("");
        }
    }
	
	 public static void main(String[] args) {
         Soal_6dinamis jawab = new Soal_6dinamis();
         jawab.showData(6);
    }
}
