package com.xsis.day5;

public class Soal_5dinamis {
	
	public int[] getFib(int n){
	        
	        int[] data = new int[n*2];
	        for(int y = 0; y < n*2; y++){
	          
	                data[y] = y;
	  
	        }
	        return data;
	    }
	
	public String[][] setData(int n){
        String[][] data = new String[n*2][n*2];
        
        int[] fib1 = getFib(n);
        
        for(int y = 0; y < n*2-1; y++){
        	int j=1;
            for(int x= 0; x < n*2-1; x++){
            	if(x+y>=n-1&&x-y<n&&y<n){        
            	data[x][y] = j + " ";
            	j++;
            	//data[x][y] = x+"."+y+" ";
            	}
            	else if(x-y>n-n*2&&x+y<((n-1)*3)+1&&y>n-1){
            		data[x][y] = j + " ";
            		j++;
            	}
            }
        }

        return data;
    }
	
	public void showData(int n){
        String[][] data = setData(n);

        for(int y = 0; y < n*2-1; y++){
            for(int x= 0; x < n*2-1; x++){
             if(data[x][y] == null){
                  data[x][y] ="  ";
                }
             System.out.print(data[x][y]+"  ");
           
            } System.out.println("");
        }
    }
	
	 public static void main(String[] args) {
         Soal_5dinamis jawab = new Soal_5dinamis();
         jawab.showData(5);
    }
}
