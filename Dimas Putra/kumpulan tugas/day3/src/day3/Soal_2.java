package day3;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Soal_2 {
	
	//set array
	public int[] setData(int n){
		int[] data = new int[n];
		//code set
		for (int y = 0; y<n;y++){
			for(int x = 0; x<n ;x++){
				if(x<3){
					data[x]=1;
					}else {data[x]=data[x-1]+data[x-2]+data[x-3];
				}
				/*if(x==y){
					data[x][y] = x + "." + y +" ";
				}else if(x+y==n-1){
					data[x][y] = x + "." + y +" ";
				}else if(y==(n-1)/2){
					data[x][y] = x + "." + y +" ";
				}else if(x==(n-1)/2){
					data[x][y] = x + "." + y +" ";
				}*/
			}	
		}
		return data;
	}
		
	//show array
	public void showData(int n){
		System.out.println("=== Menampilkan data :"+ n + " ===");
		int[] data = setData(n);
		
		for (int y = 0; y<n;y++){
			for(int x = 0; x<n ;x++){
				if(y==0){
				System.out.print(data[x]);
				}
			}
			System.out.println("");
		}
	}
	
	public static void main(String args[])throws Exception{
		InputStreamReader nilai = new InputStreamReader(System.in);
		BufferedReader nilaibr = new BufferedReader(nilai);
		System.out.println("Masukkan Angka :");
		String angka = nilaibr.readLine();
		int a = Integer.parseInt(angka);
		Soal_2 logic =new Soal_2();
		logic.showData(a);
	}
}