package day3;


public class Soal_9 {

    
    public int[] getFib(int n){
        
        int[] data = new int[n];
        int loop = 1;
        int a=0;
		int b=1;
        for(int y = 0; y < n; y++){
            if(y%2==1){ 
                data[y] = 0;
            } else {
                data[y] = b;
                b=a+b;
				a=b-a;
            }
            
            loop++;
        }
        
        return data;
    }
    
    public String[][] setData(int n){
        String[][] data = new String[n][n];
        int[] fib1 = getFib(n);
        
        for(int temp : fib1){
            System.out.println(""+ temp);
        }
        
        int max = n - 1;
        
        for(int y = 0; y < n; y++){
            for(int x= 0; x < n; x++){
                
               if( y + x >= max && x <= y) {
                        if (y%2==0){
                                data[x][y]=  fib1[n-y-1] + " ";
                        }

                }
                else if(y+x<=max && x>=y) //Atas
                {
                        if (y%2==0){
                                data[x][y]= fib1[y]+" ";
                        }
                }
                else if(y+x<=max && x<=y) //Kiri
                {
                        if (x%2==0){
                                data[x][y]= fib1[x]+" ";
                        }
                }
                else if(y+x>=max && x>=y) //Kanan
                {
                		if(x%2==0){
                        data[x][y]= fib1[n-x-1] + " ";
                		}
                } 
            }
        }
        
        
        return data;
    }
    
    public void showData(int n){
        String[][] data = setData(n);
        int max = n - 1;
        for(int y = 0; y < n; y++){
            for(int x= 0; x < n; x++){
                if(data[x][y] == null){
                    data[x][y] = "  ";
                }
                
                System.out.print(data[x][y]);
            }
            
            System.out.println("");
        }
        
    }
    
    public static void main(String[] args) {
         Soal_9 jawab = new Soal_9();
         jawab.showData(9);
    }
}
