package day3;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Soal_8 {
	
	//set array
	public String[][] setData(int n){
		String[][] data = new String[n][n];
		//code set
		int a=1;
		int b=n;
		for (int y = 0; y<n;y++){
			int c=1;
			int d=n;
			for(int x = 0; x<n ;x++){
				if(x+y>=n-1&&x>=y&&x%2==0){
	 				data[x][y] = d + " ";
				}else if(x+y>=n-1&&x<=y&&y%2==0){
					data[x][y] = b + " ";
				}else if(x+y<=n-1&&x<=y&&x%2==0){
					data[x][y] = c + " ";
				}else if(x+y<=n-1&&x>=y&&y%2==0){
					data[x][y] = a + " ";
				}c++;
				d--;
				
			}a++;
			b--;
			
			
		}
		return data;
	}
		
	//show array
	public void showData(int n){
		System.out.println("=== Menampilkan data :"+ n + " ===");
		String[][] data = setData(n);
		int ntsetengah =((n+1)/2)/2;
		int nt=(n-1)/2;
		for (int y = 0; y<n;y++){
			for(int x = 0; x<n ;x++){
				if(data[x][y] == null){
					data[x][y] = "  ";
				}
				System.out.print(data[x][y]);
			}
			System.out.println("");
		}
	}
	
	public static void main(String args[])throws Exception{
		InputStreamReader nilai = new InputStreamReader(System.in);
		BufferedReader nilaibr = new BufferedReader(nilai);
		System.out.println("Masukkan Angka :");
		String angka = nilaibr.readLine();
		int a = Integer.parseInt(angka);
		Soal_8 logic =new Soal_8();
		logic.showData(a);
	}
}
