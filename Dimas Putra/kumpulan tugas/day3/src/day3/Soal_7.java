package day3;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Soal_7 {
	
	//set array
	public String[][] setData(int n){
		String[][] data = new String[n][n];
		//code set
		int o=0;
		int s=1;
		for(int i= 0;i<n;i++){
			s=o+s;
			o=s-o;
		}
		//vibonacci terbalik
		int q=s;
		int j=o;
		
		int c=0;
		int d=1;
		for (int y = 0; y<n;y++){
			int a=0;
			int b=1;
			
			int e=s;
			int f=o;
			for(int x = 0; x<n ;x++){
				if(x+y>=n-1&&x>=y){
					data[x][y] = f + " ";
				}else if(x+y>=n-1&&x<=y){
					data[x][y] = j + " ";
				}else if(x+y<=n-1&&x<=y){
					data[x][y] = b + " ";
				}else if(x+y<=n-1&&x>=y){
					data[x][y] = d + " ";
				}
				b=a+b;
				a=b-a;
				f=e-f;
				e=e-f;
			}	
			j=q-j;
			q=q-j;
			d=c+d;
			c=d-c;
		}
		return data;
	}
		
	//show array
	public void showData(int n){
		System.out.println("=== Menampilkan data :"+ n + " ===");
		String[][] data = setData(n);
		
		for (int y = 0; y<n;y++){
			for(int x = 0; x<n ;x++){
				if(data[x][y] == null){
					data[x][y] = "  ";
				}
				System.out.print(data[x][y]);
			}
			System.out.println("");
		}
	}
	
	public static void main(String args[])throws Exception{
		InputStreamReader nilai = new InputStreamReader(System.in);
		BufferedReader nilaibr = new BufferedReader(nilai);
		System.out.println("Masukkan Angka :");
		String angka = nilaibr.readLine();
		int a = Integer.parseInt(angka);
		Soal_7 logic =new Soal_7();
		logic.showData(a);
	}
}
