package day3;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Soal_6 {
	
	//set array
	public String[][] setData(int n){
		String[][] data = new String[n][n];
		//code set
		for (int y = 0; y<n;y++){
			int a=0;
			int b=1;
			for(int x = 0; x<n ;x++){
				if(x==y){
					data[x][y] = b + " ";
				}else if(x+y==n-1){
					data[x][y] = b + " ";
				}else if(x+y>=n-1&&x>y){
					data[x][y] = "B" + " ";
				}else if(x+y>=n-1&&x<y){
					data[x][y] = "C" + " ";
				}else if(x+y<=n-1&&x<y){
					data[x][y] = "D" + " ";
				}else if(x+y<=n-1&&x>y){
					data[x][y] = "A" + " ";
				}
				b=a+b;
				a=b-a;
			}	
		}
		return data;
	}
		
	//show array
	public void showData(int n){
		System.out.println("=== Menampilkan data :"+ n + " ===");
		String[][] data = setData(n);
		
		for (int y = 0; y<n;y++){
			for(int x = 0; x<n ;x++){
				if(data[x][y] == null){
					data[x][y] = "  ";
				}
				System.out.print(data[x][y]);
			}
			System.out.println("");
		}
	}
	
	public static void main(String args[])throws Exception{
		InputStreamReader nilai = new InputStreamReader(System.in);
		BufferedReader nilaibr = new BufferedReader(nilai);
		System.out.println("Masukkan Angka :");
		String angka = nilaibr.readLine();
		int a = Integer.parseInt(angka);
		Soal_6 logic =new Soal_6();
		logic.showData(a);
	}
}
