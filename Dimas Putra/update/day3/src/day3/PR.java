package day3;


public class PR {

	public char[] getAbjad(){
		char[] abjad = new char[40];
		for(char c = 'A'; c <= 'Z';++c){
			abjad[c - 'A']=c;
		}
		return abjad;
	}
	
    public int[] getFib(int n){
    	
        int[] data = new int[n];
        for(int y = 0; y < n; y++){
           
            if(y < 2){ 
                data[y] = 1;
            } else {
                data[y] = data[y - 1] + data[y - 2];
            }
        }
        return data;
    }
    
    public String[][] setData(int n){
        String[][] data = new String[n*2-1][n];
        int[] fib1 = getFib(n);
        
        for(int temp : fib1){
            System.out.println(""+ temp);
       }
        
        int max = n*2 - 1;
        int tengah = n-1;
        
        char[] abjad = getAbjad();
        
        for(int y = 0; y < n; y++){
        	
        	int z=0;
            for(int x= 0; x < n*2-1; x++){
            	
            	// data[x][y]= x + "." + y +" ";
            	
            	  if( y + x == tengah) {
                        //if (y%2==0){
                               data[x][y]=  fib1[x] + " ";
            	
                      //  }

                }
                else if(y==x) //Atas
                {
                        //if (y%2==0){
                                data[x][y]= fib1[y]+" ";
                       // }
                }
               else if( x+y+1==max ) {
                   //if (y%2==0){
            	//   data[x][y]= x + "." + y +" ";
                     data[x][y]=  fib1[y] + " ";
                   //}

                	}
              else if (x-y==tengah) //Atas
               		{
                   //if (y%2==0){
            	//  data[x][y]= x + "." + y +" ";
                        data[x][y]= fib1[n-y-1] +" ";
                  }
              else if (y== n/2){
                  	  data[x][y] = abjad[z]+" ";
                  	  if(x>=n-1){
                  		  z--;
                  	  }else{
                  		  z++;
                  	  }
                  			  
                    }
           }
            
        }
        
        
        return data;
    }
    
    public void showData(int n){
        String[][] data = setData(n);

        for(int y = 0; y < n; y++){
            for(int x= 0; x < n*2-1; x++){
             if(data[x][y] == null){
            	 // System.out.print(data[x][y]+" ");
                  data[x][y] = "  ";
                }
               //if(data[x][y] == null){
               System.out.print(data[x][y]+"  ");
            //    }
            }
            
            System.out.println("");
        }
        
    }
    
    public static void main(String[] args) {
         PR jawab = new PR();
         jawab.showData(9);
    }
}
