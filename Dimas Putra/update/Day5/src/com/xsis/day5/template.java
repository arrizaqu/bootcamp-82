package com.xsis.day5;

public class template {

	public String[][] setData(int n){
        String[][] data = new String[n*2][n];
        
        for(int y = 0; y < n; y++){
            for(int x= 0; x < n*2-1; x++){
            	data[x][y] = x+"."+y+" ";
            }
        }

        return data;
    }
	
	public void showData(int n){
        String[][] data = setData(n);

        for(int y = 0; y < n; y++){
            for(int x= 0; x < n*2-1; x++){
             if(data[x][y] == null){
                  data[x][y] = "  ";
                }
             System.out.print(data[x][y]+"  ");
           
            } System.out.println("");
        }
    }
	
	 public static void main(String[] args) {
         template jawab = new template();
         jawab.showData(5);
    }
}
