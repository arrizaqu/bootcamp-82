package com.xsis.day5;

public class Soal_10dinamis {
	
	public int[] getFib(int n){ 				// membuat array fibonacci
	        
	        int[] data = new int[n*4];
	    
	        for(int y = 0; y < n*2; y++){
	        	   if(y<3){ 
	                   data[y] = 1;
	               } else {
	                   data[y] = data[y-1]+data[y-2];
	             
	               }
	        }
	        return data;
	    }
	
	public String[][] setData(int n){   		// mengisi data ke array
        String[][] data = new String[n*4][n*2]; // string baru untuk menyimpan array
        int[] fib1 = getFib(n);    				// mengambil data dari fungsi array
        
        for(int y = 0; y < n*2-1; y++){   		// perulangan y
        	int g=0;
        	int j=0;
            for(int x= 0; x < n*4; x++){  	//perulangan x
            	
            	//data[x][y] = x+"."+y+" ";    	//mengecek kamus data
     
            	
            if(x-y==n-1&&x<n*2-2&&y>=1){
            	 data[x-(((n-1)/2)+((n-1)/2))+1+((n-1)*2)][y+n-2] = fib1[x-(n-1)] + " ";  //kotak kecil kiri bawah duplikat
              	 data[x-(((n-1)/2)+((n-1)/2))+1][y+n-2] = fib1[x-(n-1)] + " ";  //kotak kecil kiri bawah
              	 data[x+((n-1)*2)][y+2] = fib1[n-y-2] + " ";  					//kotak kecil kanan atas duplikat
              	 data[x][y+2] = fib1[n-y-2] + " ";  							//kotak kecil kanan atas
              	 data[x][y] = fib1[n-y-1+1] + " ";  							//kanan atas
               	 data[x+(n-1)*2][y] = fib1[n-y-1+1] + " ";  					//duplikat
               	 data[x-(n-1)][y+(n-1)] = fib1[x-(n-1)+1] + " "; 				//kiri bawah
               	 data[x-(n-1)+((n-1)*2)][y+(n-1)] = fib1[x-(n-1)+1] + " "; 		//duplikat
               }	
            
            else if(x+y==n-1){       	
          		data[x+(n/2)+(n/2)-1][y+((n-1)/2)+(n/2)-1] = fib1[y] + " "; 			//kotak dalem kanan bawah 
          		data[x+((n-1)*2)+((n-1)/2)+(n/3)][y+((n-1)/2)+(n/2)-1] = fib1[y] + " "; //kotak dalem kanan bawah duplikat
          		data[x+1+((n-1)*2)][y+1] = fib1[x] + " "; 								//kotak dalem kiri atas
          		data[x+1][y+1] = fib1[x] + " "; 										//kotak dalem kiri atas duplikat
           	data[x][y] = fib1[x+1] + " "; 											//kiri atas
           	data[x+(n-1)*2][y] = fib1[x+1] + " "; 									//duplikat
            	data[x+n-1][y+n-1] = fib1[y+1] + " ";   								//kanan bawah
            	data[x+n-1+((n-1)*2)][y+n-1] = fib1[y+1] + " "; 						//duplikat
           
          	}
            else if(x+y==n-4){ 
            	data[x+n-1][y+n-2] = fib1[y+2] + " "; 	
            	data[x+3][y+4] = fib1[x+2] + " "; 	
            	data[x+3+((n-1)*2)][y+4] = fib1[x+2] + " "; 
           	data[x+n-1+((n-1)*2)][y+n-2] = fib1[y+2] + " "; 	
            }
            
            else if(x-y==n&&x<(n*2)-(n/2)&&y>1){
            	data[x-2][y+3] = fib1[n-y-1] + " "; 
            	data[x-2+(n*2)-2][y+3] = fib1[n-y-1] + " "; 
            	data[x-(n/3)*2][y+n-2] = fib1[y+2] + " "; 
            	data[x-((n/3)*2)+(n*2)-2][y+n-2] = fib1[y+2] + " "; 
            }
            
            }
        }

        return data;
    }
	
	public void showData(int n){       			//menampilkan data dari setData
        String[][] data = setData(n);   		// mengambil variable dari set data

        for(int y = 0; y < n*2-1; y++){   		// perulangan y
            for(int x= 0; x < n*4; x++){ 		//perulangan y
             if(data[x][y] == null){ 			//memunculkan spasi jika data kosong
                  data[x][y] ="  ";
                }
             System.out.print(data[x][y]+"\t"); //menampilkan data
           
            } System.out.println("");
        }
    }
	
	 public static void main(String[] args) {
         Soal_10dinamis jawab = new Soal_10dinamis();	// membuat variable baru
         jawab.showData(9);								// mengirim data ke show data
    }
}
