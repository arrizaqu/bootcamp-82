package com.xsis.day5;

public class Soal_4dinamis {
	
	public int[] getFib(int n){
	        
	        int[] data = new int[n*2];
	        for(int y = 0; y < n*2; y++){
	          if(y%2==1){
	                data[y] = y;
	          }
	        }
	        return data;
	    }
	
	public String[][] setData(int n){
        String[][] data = new String[n*2][n];
        
        int[] fib1 = getFib(n);
        
        for(int y = 0; y < n; y++){
        	int j=1;
            for(int x= 0; x < n*2-1; x++){
            	if(x>=y&&x+y<n*2-1){        
            	data[x][y] = j + " ";
            	j++;
            	}else{}
            }
        }

        return data;
    }
	
	public void showData(int n){
        String[][] data = setData(n);

        for(int y = 0; y < n; y++){
            for(int x= 0; x < n*2-1; x++){
             if(data[x][y] == null){
                  data[x][y] ="  ";
                }
             System.out.print(data[x][y]+"  ");
           
            } System.out.println("");
        }
    }
	
	 public static void main(String[] args) {
         Soal_4dinamis jawab = new Soal_4dinamis();
         jawab.showData(5);
    }
}
