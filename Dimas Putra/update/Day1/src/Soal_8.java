
public class Soal_8 {
	
	public static void main(String[] args){
		Soal_8 soal_1 = new Soal_8();
		soal_1.deretBintang();
	}
	
	public void deretBintang(){
		int x=9;
		int tengah=(x+1)/2;
		for(int i=0;i<x;i++){
			for(int j=0;j<x;j++){
				if (j<tengah){
					if (i>=j&&i<=x-(j+1)){
						System.out.print("* ");
					}else System.out.print("  ");
				} else if(i<=j&&i>=x-(j+1)){
					System.out.print("* ");
				}else System.out.print("  ");
			}System.out.println();
		}
	}
}
