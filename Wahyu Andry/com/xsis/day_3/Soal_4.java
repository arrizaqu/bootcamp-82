package com.xsis.day_3;
import java.util.Scanner;

public class Soal_4 {
	private static Scanner sc;
	public static void main(String[] args) {
	sc = new Scanner(System.in);
	System.out.print("Masukkan bilangan: ");
	int n = sc.nextInt();
	
	for(int y = 1; y <= n; y++){
        int counter = 0; 
        for(int x = 1; x <= y; x++){
              if(y % x == 0){ 
                    counter++;
              }
        }
      
      if(counter == 2){
          
             System.out.println(y + " adalah bilangan prima");
      }
	}
	
	}

}
