package com.xsis.day_3;
import java.util.Scanner;

public class Soal_8 {
	
	private static Scanner sc;

	// set array
	public String[][] setData(int n){
		String[][] data = new String[n][n];
		//code set
		int max = n-1, nilai_tengah = max/2, i = n;	
		
		for (int y=0; y<n; y++){
			int j = n;
			for (int x = 0; x < n; x++){
				if (x > y && x <= n-y-1 && y % 2 == 0){
					data[x][y] = y + 1 +"  ";
				}
				else if (x < y && x <= n-y-1 && x % 2 == 0 || x == y && y <= nilai_tengah && x % 2 == 0){
					data[x][y] = x + 1 +"  ";
				}
				else if (x < y && x >= n-y-1 && i % 2 == 1  || x == y && y >= nilai_tengah && i % 2 == 1){
					data[x][y] = i +"  ";
				}
				else if (x > y && x >= n-y-1 && j % 2 == 1){
					data[x][y] = j +"  ";
				}
				j = j-1;
				
			}
			i = i-1;
			
		}
		
		
		return data;
	}
	
	// show array
	public void showData(int n) {
		System.out.println("=== Menampilkan data :"+ n + " === ");
		String [][] data =  setData(n);
		
		for (int y = 0; y < n; y++){
			for (int x = 0; x < n; x++){
				if(data[x][y] == null){
					data[x][y] = "   ";
				}
				System.out.print(data[x][y]);
			}
			System.out.println("");
		}
	}
	
	public static void main(String[] args) {
		sc = new Scanner(System.in);
		System.out.print("Masukkan nilai: ");
	    int n = sc.nextInt();
		
		Soal_8 logic = new Soal_8();
		logic.showData(n);

	}

}
