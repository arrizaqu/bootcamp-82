package com.xsis.day_3;
import java.util.Scanner;

public class Soal_1 {
	
	private static Scanner sc;

	//set array
		public int[] setData(int n){
			int[] data = new int[n];
			
			//set code
			for(int y=0; y<n; y++){
				if(y<2){
					data[y]=1; }
				else{
				data[y]=data[y-1]+data[y-2];}				
			}
			
			return data;
			
		}
		
		//show array
		public void showData(int n){
			System.out.print("Menampilkan fibonaci \n");
			int[] data =  setData(n);
			
			for (int y = 0; y < n; y++){
				
					System.out.print(data[y] + " ");
					
//				System.out.println("");
			}
		}
		
	
	public static void main(String[] args) {	
		sc = new Scanner(System.in);
		System.out.print("Masukkan nilai: ");
	    int n = sc.nextInt();
	    
		Soal_1 logic = new Soal_1();
		logic.showData(n);

	}

}
