package com.xsis.day_3;
import java.util.Scanner;

public class Soal_7 {
	
	private static Scanner sc;

	// set array
	public String[][] setData(int n){
		String[][] data = new String[n][n];
		int[] fibbonacci = new int[n];
		//code set
		int max = n-1, nilai_tengah = max/2;		

        for (int i = 0; i <= nilai_tengah; i++)
        {
            if (i < 2) { fibbonacci[i] = 1; }
            else { fibbonacci[i] = fibbonacci[i - 1] + fibbonacci[i - 2]; }
            fibbonacci[(n - 1) - i] = fibbonacci[i];
        }
        
		for (int y = 0; y < n; y++){
			
			
			for (int x = 0; x < n; x++){	

				if (x == y && x <= nilai_tengah){
					data[x][y] = fibbonacci[x]+ "  ";
				}else if (x == y && x > nilai_tengah) {
					data[x][y] = fibbonacci[x]+ "  ";
				}else if (x == max - y && x < nilai_tengah) {
					data[x][y] = fibbonacci[x]+ "  ";
				}else if (x == max - y && x > nilai_tengah) {
					data[x][y] = fibbonacci[x]+ "  ";
				}else if (x > y && x + y < max) {
					data[x][y] = fibbonacci[y]+ "  ";
				}else if (x > y && x + y > max) {
					data[x][y] = fibbonacci[x]+ "  ";
				}else if (x < y && x + y > max) {
					data[x][y] = fibbonacci[y]+ "  ";
				}else if (x < y && x + y < max) {
					data[x][y] = fibbonacci[x]+ "  ";
				}

			}
		}
		
		return data;
	}
	
	// show array
	public void showData(int n) {
		System.out.println("=== Menampilkan data :"+ n + " === ");
		String [][] data =  setData(n);
		
		for (int y = 0; y < n; y++){
			for (int x = 0; x < n; x++){
				if(data[x][y] == null){
					data[x][y] = "   ";
				}
				System.out.print(data[x][y]);
			}
			System.out.println("");
		}
	}
	
	public static void main(String[] args) {
		sc = new Scanner(System.in);
		System.out.print("Masukkan nilai: ");
	    int n = sc.nextInt();
		
		Soal_7 logic = new Soal_7();
		logic.showData(n);

	}

}
