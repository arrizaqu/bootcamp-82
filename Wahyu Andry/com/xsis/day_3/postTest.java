package com.xsis.day_3;
import java.util.Scanner;

public class postTest {
	
	private static Scanner sc;

	// set array
	public String[][] setData(int n){
		String[][] data = new String[n*2-1][n];
		int[] fibbonacci = new int[n];
		
		//code set
		 int max = n-1, nilai_tengah = max/2;

		 for (int i = 0; i < n ; i++)
         {
             if (i < 2) { fibbonacci[i] = 1; }
             else { fibbonacci[i] = fibbonacci[i - 1] + fibbonacci[i - 2]; }
//             fibbonacci[(n-1)-i] = fibbonacci[i];
         }
		
	
		for (int y = 0; y < n; y++){
			
			for (int x = 0; x < n*2-1; x++){
				if(x == y){
					data[x][y] = fibbonacci[x]+ "  ";
				}else if(x == max-y){
					data[x][y] = fibbonacci[x]+ "  ";
				}else if(x + y == max*2){
					data[x][y] = fibbonacci[y]+ "  ";
				}else if(x - y == max){
					data[x][y] = fibbonacci[n-y-1]+ "  ";
				}
								
			}
			
		}
		
		return data;
	}
	
	// show array
	public void showData(int n) {
		System.out.println("=== Menampilkan data :"+ n + " === ");
		String [][] data =  setData(n);
		
		for (int y = 0; y < n; y++){
			for (int x = 0; x < n*2-1; x++){
				if(data[x][y] == null){
					data[x][y] = "   ";
				}
				System.out.print(data[x][y]);
			}
			System.out.println("");
		}
	}
	
	public static void main(String[] args) {
		sc = new Scanner(System.in);
		System.out.print("Masukkan nilai: ");
	    int n = sc.nextInt();
		
		postTest logic = new postTest();
		logic.showData(n);

	}

}
