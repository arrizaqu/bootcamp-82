package com.xsis.day_3;
import java.util.Scanner;

public class Soal_5 {
    private static Scanner sc;

	public static int hitungFPB(int x, int y) {
        if (x > y) {
            if (x % y == 0) {
                return y;
            } else {
                return hitungFPB(y, x % y); 
            }
        } else { 
            if (y % x == 0) { 
                return x; 
            } else {
                return hitungFPB(x, y % x); 
            }
        }
    }
 
    public static void main(String[] args) {
    	sc = new Scanner(System.in);
    	System.out.print("Masukkan bilangan pertama: ");
        int m = sc.nextInt();
		System.out.print("Masukkan bilangan kedua: ");
	    int n = sc.nextInt();
	    
	    int hasil = hitungFPB(m, n);
	    
        System.out.println("FPB dari kedua bilangan tersebut adalah : "+hasil); 
    }
}