package com.xsis.day_5;
import java.util.Scanner;

public class Soal_7 {
	
	private static Scanner sc;
	public int[] getAngka(int n){
		int[] ax = new int[n*2 - 1];
		
		for (int y = 0; y < n*2 - 1; y++){
			if (y == 0)
				ax[y] = 1;
			else
				ax[y] = ax[y - 1] + 2;
		}
		return ax;
	}


	// set array
	public String[][] setData(int n){
		String[][] data = new String[n*2-1][n];
		int[] angka = getAngka(n);
		
		//code set
		int max = n-1, nilai_tengah = max/2;		
	
		for (int y = 0; y < n; y++){
			int i = 0;
			for (int x = 0; x < n*2-1; x++){
				if (x == y || x + y == max*2 || x > y && x <= (n*2-1)- y -1){
					data[x][y] = angka[i]+"  ";
					if (x < max)
					i++;	
					else i--;
				}
				
			}
			
		}
		
		return data;
	}
	
	// show array
	public void showData(int n) {
		System.out.println("=== Menampilkan data :"+ n + " === ");
		String [][] data =  setData(n);
		
		for (int y = 0; y < n; y++){
			for (int x = 0; x < n*2-1; x++){
				if(data[x][y] == null){
					data[x][y] = "   ";
				}
				System.out.print(data[x][y]);
			}
			System.out.println("");
		}
	}
	
	public static void main(String[] args) {
		sc = new Scanner(System.in);
		System.out.print("Masukkan nilai: ");
	    int n = sc.nextInt();
		
		Soal_7 logic = new Soal_7();
		logic.showData(n);

	}

}