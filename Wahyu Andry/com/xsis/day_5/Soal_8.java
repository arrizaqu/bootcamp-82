package com.xsis.day_5;
import java.util.Scanner;

public class Soal_8 {
	
	private static Scanner sc;
	
	public int[] getAngka(int n){
		int[] dt = new int[n];
		
		for (int y =0; y<n; y++){
			if (y==0)
				dt[y]=1;
			else
				dt[y]=dt[y-1]+2;
		}
		return dt;
	}

	// set array
	public String[][] setData(int n){
		String[][] data = new String[n][n];
		int[] angka = getAngka(n);
		//code set
		int max = n-1, nilai_tengah = max/2;		
	
		for (int y = 0; y < n; y++){
			int a = 0;
			for (int x=0; x<n; x++){
				if (x-y<=nilai_tengah && x+y >=nilai_tengah && y-x<=nilai_tengah && y+x<n+nilai_tengah){
					data[x][y] = angka[a]+"  ";
					if (x<nilai_tengah){
					a++;}
					
					else{ a--;}
				}
				
			}
			
		}
		
		return data;
	}
	
	// show array
	public void showData(int n) {
		System.out.println("=== Menampilkan data :"+ n + " === ");
		String [][] data =  setData(n);
		
		for (int y = 0; y < n; y++){
			for (int x = 0; x < n; x++){
				if(data[x][y] == null){
					data[x][y] = "   ";
				}
				System.out.print(data[x][y]);
			}
			System.out.println("");
		}
	}
	
	public static void main(String[] args) {
		sc = new Scanner(System.in);
		System.out.print("Masukkan nilai: ");
	    int n = sc.nextInt();
		
		Soal_8 logic = new Soal_8();
		logic.showData(n);

	}

}