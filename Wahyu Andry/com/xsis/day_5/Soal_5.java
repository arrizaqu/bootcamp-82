package com.xsis.day_5;
import java.util.Scanner;

public class Soal_5 {
	
	private static Scanner sc;

	// set array
	public String[][] setData(int n){
		String[][] data = new String[n][n];
		
		//code set
		int max = n-1, nilai_tengah = max/2;		
	
		for (int y = 0; y < n; y++){
			int angka1 = 1;
			int angka_tengah = 1;
			
			for (int x = 0; x < n; x++){
				
				if(y == nilai_tengah){
					data[x][y] = angka_tengah + "  ";	
				}else if(x + y >= nilai_tengah && x - y <= nilai_tengah){
					if(x + y <= max*2-nilai_tengah && y - x <= nilai_tengah){
					data[x][y] = angka1 + "  ";
					angka1++;
					}
				}
				
				angka_tengah++;
						
				
			}
			
		}
		
		return data;
	}
	
	// show array
	public void showData(int n) {
		System.out.println("=== Menampilkan data :"+ n + " === ");
		String [][] data =  setData(n);
		
		for (int y = 0; y < n; y++){
			for (int x = 0; x < n; x++){
				if(data[x][y] == null){
					data[x][y] = "   ";
				}
				System.out.print(data[x][y]);
			}
			System.out.println("");
		}
	}
	
	public static void main(String[] args) {
		sc = new Scanner(System.in);
		System.out.print("Masukkan nilai: ");
	    int n = sc.nextInt();
		
		Soal_5 logic = new Soal_5();
		logic.showData(n);

	}

}