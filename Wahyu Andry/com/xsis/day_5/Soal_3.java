package com.xsis.day_5;
import java.util.Scanner;

public class Soal_3 {
	
	private static Scanner sc;

	// set array
	public String[][] setData(int n){
		String[][] data = new String[n*2-1][n];
		
		//code set
		int max = n-1, nilai_tengah = max/2;		
	
		for (int y = 0; y < n; y++){
			int angka1 = 1;
			
			for (int x = 0; x < n*2-1; x++){
				
				if (x + y >= n-1 && x - y < n && x + y > max-1 && x - y < n){
					data[x][y] = angka1 + "  ";
					angka1++;
				}
									
			}		
			
		}
		
		return data;
	}
	
	// show array
	public void showData(int n) {
		System.out.println("=== Menampilkan data :"+ n + " === ");
		String [][] data =  setData(n);
		
		for (int y = 0; y < n; y++){
			for (int x = 0; x < n*2-1; x++){
				if(data[x][y] == null){
					data[x][y] = "   ";
				}
				System.out.print(data[x][y]);
			}
			System.out.println("");
		}
	}
	
	public static void main(String[] args) {
		sc = new Scanner(System.in);
		System.out.print("Masukkan nilai: ");
	    int n = sc.nextInt();
		
		Soal_3 logic = new Soal_3();
		logic.showData(n);

	}

}
