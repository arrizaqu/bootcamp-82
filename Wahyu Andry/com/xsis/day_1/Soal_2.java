package com.xsis.day_1;

public class Soal_2 {

	public static void main(String[] args) {
		Soal_2 soal_2 = new Soal_2();
		soal_2.deret_bintang();

	}
	
	public void deret_bintang(){
		 for (int baris = 1; baris <= 9; baris++)
         {
             for (int kolom = 1; kolom <= 9; kolom++)
             {
                 if (baris + kolom == 10) { System.out.print("* "); }
                 else { System.out.print("  "); }
             }
             System.out.println();
         }
	}

}
