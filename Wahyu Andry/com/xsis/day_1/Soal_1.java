package com.xsis.day_1;

public class Soal_1 {

	public static void main(String[] args) {
		Soal_1 soal_1 = new Soal_1();
		soal_1.deret_bintang();
	}
	
	public void deret_bintang(){
		for (int baris = 1; baris <= 9; baris++)
        {
            for (int kolom = 1; kolom <= 9; kolom++)
            {
                if (baris == kolom) { System.out.print("* "); }
                else { System.out.print("  "); }
            }
            System.out.println();
        }
	}

}
