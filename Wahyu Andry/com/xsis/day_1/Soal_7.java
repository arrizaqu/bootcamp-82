package com.xsis.day_1;

public class Soal_7 {

	public static void main(String[] args) {
		Soal_7 Soal_7 = new Soal_7();
		Soal_7.deret_bintang();

	}
	
	public void deret_bintang(){
		for (int baris = 1; baris <= 9; baris++)
        {
            for (int kolom = 1; kolom <= 9; kolom++)
            {
                if (baris <= 5) 
                {
                    if ((kolom >= baris) && (kolom <= (9 - (baris - 1)))) { System.out.print("* "); }
                    else { System.out.print("  "); }
                }
                else 
                {
                    if ((kolom >= (9-(baris-1)) && (kolom <= baris ))) { System.out.print("* "); }
                    else { System.out.print("  "); }
                }
            }
            System.out.println();
        }
	}

}