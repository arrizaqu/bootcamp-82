package com.xsis.day_1;

public class Soal_3 {

	public static void main(String[] args) {
		Soal_3 Soal_3 = new Soal_3();
		Soal_3.deret_bintang();
	}
	
	public void deret_bintang(){
		for (int baris = 1; baris <= 9; baris++)
        {
            for (int kolom = 1; kolom <= 9; kolom++)
            {
                if (baris + kolom == 10) { System.out.print("* "); }
                else if (baris == kolom) { System.out.print("* "); }
                else { System.out.print("  "); }
            }
            System.out.println();
        }
		
	}

}
