package com.xsis.day_1;

public class Soal_6 {

	public static void main(String[] args) {
		Soal_6 Soal_6 = new Soal_6();
		Soal_6.deret_bintang();

	}
	
	public void deret_bintang(){
	    for (int baris = 9; baris >= 1; baris--)
        {
            for (int kolom = 1; kolom <= 9 ; kolom++)
            {
                if (kolom < baris) { System.out.print("  "); }
                else { System.out.print("* "); }
            }
            System.out.println();
        }
	}

}
