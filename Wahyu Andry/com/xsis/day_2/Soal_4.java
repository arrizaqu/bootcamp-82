package com.xsis.day_2;

public class Soal_4 {

	public static void main(String[] args) {
		int x = 16, z = 1, max = 9, nilai_tengah = (max+1)/2;
		for (int baris = 1; baris <= max; baris++)
        {
			int y = 0;
            for (int kolom = 1; kolom <= max; kolom++)
            {
               if(baris == kolom){
            	   System.out.print(z+" ");
               }else if(baris==max-kolom+1){
            	   System.out.print(x+ " ");
               }else if(baris == nilai_tengah){
            	   System.out.print(y+" ");
               }else if(kolom == nilai_tengah){
            	   System.out.print(z+" ");
               }
               else{
            	   System.out.print("  ");
               }
               
               y+=2;
            }
            x-=2;
            z+=2;
            System.out.println("");
        }
	}

}
