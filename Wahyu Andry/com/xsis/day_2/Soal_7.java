package com.xsis.day_2;

public class Soal_7 {

	public static void main(String[] args) {
		int angka1 = 1 , angka2 = 16, max = 9, nilai_tengah = (max+1)/2;
		for(int y = 1; y <= max; y++){
			for(int x = 1; x <= max; x++){
				if(x == y){
					System.out.print(angka1);
				}else if(x + y == max + 1 && y < nilai_tengah){
					System.out.print(angka2);
				}else if(x + y == max + 1 && y > nilai_tengah){
					System.out.print(angka2);
				}else if(x > y && x + y < max+1){
					System.out.print(" A ");
				}else if(x < y && x + y > max+1){
					System.out.print(" B ");
				}
				else{
					System.out.print("   ");
				}
			}
			angka1 +=2;
			angka2 -=2;
			System.out.println();
		}

	}

}
