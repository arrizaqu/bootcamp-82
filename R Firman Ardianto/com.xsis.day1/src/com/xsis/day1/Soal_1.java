package com.xsis.day1;
import java.util.Scanner;

public class Soal_1
{
	public void soal1()
	{

	int bintang;
	Scanner scanner = new Scanner(System.in);

	System.out.println("Enter value : ");
	bintang = scanner.nextInt();

	for(int i = 1; i <= bintang; i++) 
	{
 
            for(int j = 1; j <= i; j++) 
            {
 
                if(j != i) 
                {
 
                    System.out.print(" ");
 
                }
                else if(j == i)
                {
 
                    System.out.println("*");
                }
            }
            System.out.println();
        }
	
    }

    
	public static void main (String ars[])
	{
	Soal_1 soal_1 = new Soal_1();
	soal_1.soal1();
	}
}