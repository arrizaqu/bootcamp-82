package com.xsis.day4;

public class Soal_1 {

	
public int[] getFib(int n){
        
        int[] data = new int[n];
        int loop = 1;
        for(int y = 0; y < n; y++){
           
            if(y < 2){ 
                data[y] = 1;
            } else {
                data[y] = data[y - 1] + data[y - 2];
            }
            
            loop++;
        }
        
        return data;
    }
	

		public static void main (String args[])
			{
				Soal_1 day4 = new Soal_1();
				day4.getFib(9);
}

	
}
