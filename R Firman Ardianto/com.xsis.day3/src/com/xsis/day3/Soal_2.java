package com.xsis.day3;
import java.util.Scanner;

public class Soal_2 {

	// set array
	public int[] setData(int n)
	{
		//Scanner scanner = new Scanner(System.in);
		//System.out.println("Enter value : ");
		//n = scanner.nextInt();
		int[] data = new int[n];
		//code set
			for (int x=0; x<n;x++)
			{
				if (x<3)
					{
					data[x] = 1;
					}
				else
					{
					data[x] = data[x-1]+ data[x-2]+data[x-3];
					}
			}	
			return data;		
	}

	// show array
	public void showData(int n)
	{
		System.out.println("==== Menampilkan data : "+ n+ " ====");
		int[] data = setData(n);
		
		for (int x=0;x<n;x++)
		{
				System.out.print(data[x]+ " ");
			}
			System.out.println();
	}
	
	public static void main (String args[])
	{
		Soal_2 day2 = new Soal_2();
		day2.showData(9);
		//InputStreamReader isr = new InputStreamReader;
		//BufferedReader Br= new BufferedReader(isr);
		//System.out.println("Memasukkan angka: ");
		//String angka = Br.readLine();
		//int n = Integer.parseInt(angka);
		//Show(n);
	}
}

