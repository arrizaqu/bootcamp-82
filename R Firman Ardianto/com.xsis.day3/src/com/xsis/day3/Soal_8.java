package com.xsis.day3;

public class Soal_8 {

	
	public int[] getFib(int n){
        
        int[] data = new int[n];
        for(int y = 0; y < n; y++){
           int loop = 1;
            if(y < 2){ 
                data[y] = 1;
            } else {
                data[y] = data[y - 1] + data[y - 2];
            }
            loop++;
        }
        
        return data;
    }
	
	public String[][] setData(int n)
	{
		String[][] data = new String[n][n];
		int[] fib1 = getFib(n);
		int max = n-1;
		//code set
			for (int y=0; y<n;y++)
				{
				for(int x=0; x<n;x++)
					{
					if(x+y>=max && x<=y) // bawah 
					{	if(y%2==0)
						{
						data[x][y] = fib1[n-y-1]+"\t";
						}
					}
					else if(x+y<= max && x>=y)// atas 
					{
						if(y%2==0)
						{
							data[x][y]= fib1[y]+"\t";
						}
					}
					else if(x+y<=max && x<=y) // kiri
					{
						if(x%2==0)
						{
							data[x][y]= fib1[x]+"\t";
						}
					}
					else if(x+y>=max && x>=y)// kanan
					{
						if(x%2==0)
						{
							 data[x][y]= fib1[n-x-1] + "\t";
						}
					}
					/*else if(x+y<=max && y>=x)
					{
						data[x][y] = b+" ";
					}
					if (x==y)
	    			{
						data[x][y] = b+ " \t";
	    			}
					else if (x+y==max && y<=max)
					{
						data[x][y] = b+ " \t";
					}
					else if(x>y && x<=n-y-1)
	    			{
	    					data[x][y] = "A \t";
	    			}
					else if (x<y && x>n-y-1)
	    			{
							data[x][y] = "C \t";
	    			}
					else if(x>y)
					{
						data[x][y] =  "B \t";
					}
					*/
				}
				}
		return data;		
	}
			public void showData(int n)
			{
				System.out.println("==== Menampilkan data : "+ n+ " ====");
				String[][] data = setData(n);
				
				for (int y=0; y<n;y++)
				{
					for (int x=0;x<n;x++)
					{
						if(data[x][y]==null)
						{
							data[x][y] = " \t";
						}
						System.out.print(data[x][y]+ "\t");
					}
					System.out.println();
				}
			}
			
			public static void main (String args[])
			{
				Soal_8 day3 = new Soal_8();
				day3.showData(17);
				//InputStreamReader isr = new InputStreamReader(System.in);
				//BufferedReader Br= new BufferedReader(isr);
				//System.out.println("Memasukkan angka: ");
				//String angka = Br.readLine();
				//int n = Integer.parseInt(angka);
				//show(n);
			}

}