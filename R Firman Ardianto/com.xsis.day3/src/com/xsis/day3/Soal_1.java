package com.xsis.day3;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Soal_1 {

	// set array
	public String[][] setData(int n)
	{
		//Scanner scanner = new Scanner(System.in);
		//System.out.println("Enter value : ");
		//n = scanner.nextInt();
		String[][] data = new String[n][n];
		int max = n-1;
		int tengah = max/2;
		int a=0;
		int b=1;
		//code set
			for (int y=0; y<n;y++)
				{
				for(int x=0; x<n;x++)
					{
					if (x==max%2)
					{
					data[y][x] = b+ " ";
					}
					}
				b=a+b;
				a=b-a;
				}
		return data;		
	}

	// show array
	public void showData(int n)
	{
		System.out.println("==== Menampilkan data : "+ n+ " ====");
		String[][] data = setData(n);
		
		for (int y=0; y<n;y++)
		{
			for (int x=0;x<n;x++)
			{
				if(data[x][y]==null)
				{
					data[x][y] = "    ";
				}
				System.out.print(data[x][y]+ " ");
			}
			System.out.println();
		}
	}
	
	public static void main (String args[])
	{
		Soal_1 day2 = new Soal_1();
		day2.showData(9);
		//InputStreamReader isr = new InputStreamReader(System.in);
		//BufferedReader Br= new BufferedReader(isr);
		//System.out.println("Memasukkan angka: ");
		//String angka = Br.readLine();
		//int n = Integer.parseInt(angka);
		//show(n);
	}
}

