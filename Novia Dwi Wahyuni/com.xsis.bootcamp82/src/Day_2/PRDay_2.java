/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Day_2;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 *
 * @author asus
 */
public class PRDay_2 {
    //~~ Array 2 Dimensi Fibonaci ~~
	
	
		//set Array
		public String[][] setData(int n){ // Nilai Array
			String[][] data = new String[n][n];
			//code set
			int max= n-1;
			int nt = max/2;
			//Deret Fibbonaci 1
			int angka1=1;
			int angka2=0;
			//Cara Fibonaci Max (Diagonal)
			int angka11=1;
			int angka22=0;
			for(int i=0;i<n;i++){
				angka11=angka11+angka22;
				angka22=angka11-angka22;
			}
			//Deret Terbaliknya
			int fibmax=angka11;
			int fibshow=angka22;
			
			for(int y=0;y<n;y++)
			{	
				
				//Fibbonaci 2 diagonal Kiri
				int fibkiri1=1;
				int fibkiri2=0;
				for(int  x=0;x<n;x++)
				{
					if(x == nt){ // Nilai Tengah - Vertikal
						data[x][y]=angka1+" ";
					}
					else if ((y==x) && (y>nt))// Diagonal kanan
					{
						data[x][y]=fibshow+" ";
					}
					else if ((y>=x) && (y+x==max))// Diagonal kiri
					{
						data[x][y]=fibkiri1+" ";
					}
					
					fibkiri1=fibkiri1+fibkiri2;
					fibkiri2=fibkiri1-fibkiri2;
				}
				angka1=angka1+angka2;
				angka2=angka1-angka2; 
				fibshow=fibmax-fibshow;
				fibmax=fibmax-fibshow;
			}
			return data;
		}
		
		//Show Array
		public void showData(int n){ // Hanya menampilkan
			System.out.println("== Menampilkan data : "+ n +" ==");
			String[][] data = setData(n);
			
			for(int y=0; y<n; y++){
				for(int x=0; x<n; x++){
					if(data[x][y]==null)
					{
						data[x][y]="  ";
					}
					System.out.print(data[x][y]);
				}
				System.out.println("");
			}
		}
		
		public static void  main(String ars[])throws Exception{ //subrutin 
			InputStreamReader nilai = new InputStreamReader(System.in);
			BufferedReader br = new BufferedReader(nilai);
			System.out.println("Masukan Angka : ");
			String angka = br.readLine();
			int n = Integer.parseInt(angka);
			//show(n);
			PRDay_2 logic = new PRDay_2();
			logic.showData(n); // set nilai yang ditampilkan
		}
}
