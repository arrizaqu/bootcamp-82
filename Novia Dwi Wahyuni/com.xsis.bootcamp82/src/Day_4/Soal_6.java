/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Day_4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author asus
 */
public class Soal_6 {
    public static void main(String ars[]) throws IOException
    {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        System.out.println("Masukan Angka : ");
        String angka = br.readLine();
        int n = Integer.parseInt(angka);
        Soal_6 day4 = new Soal_6();
        day4.showData(n);
    }
	
	public void showData(int n)
    {
        String[][] data = setData(n);
        System.out.println("== Menampilkan data : "+ n +" ==");
       
        for(int y = 0; y < n; y++)
        {
            for(int x= 0; x < (n*2)-1; x++)
            {
                if(data[x][y] == null)
                {
                    data[x][y] =  "\t";//x +"."+ y +
                }
                System.out.print(data[x][y]);
            }
            
            System.out.println("\t");
        }
    }
	public int[] getAngka(int n)
    {
        int[] dtfib = new int[n*2-1]; // batas nilai sebanyak nilai n
        //Get 

        for(int y = 0; y < (n*2)-1; y++)
        {
            if(y == 0 )
                dtfib[y]=1;
            else
                dtfib[y] = dtfib[y - 1] + 2;
        }
        return dtfib;
    }
	
	public String[][] setData(int n)
    {
        String[][] data= new String[n*2-1][n];
        int[] angka= getAngka(n);
        int max = n-1;
        
    	//int a= n-(max*2);//nilai Spasi index
        for(int y =0; y < n; y++)
        {
        	int a=0;
            for(int x = 0; x <(n*2)-1; x++)
            {
                 if(x+y>=max && x-y<=max){
                	data[x][y]=angka[a]+"\t";
                	if (x<max)
                		a++;
                	else 
                		a--;
                }
            }
            
        } 
		return data;
    }
}
