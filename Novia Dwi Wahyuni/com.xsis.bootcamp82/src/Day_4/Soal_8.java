/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Day_4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author asus
 */
public class Soal_8 {
    public static void main(String ars[]) throws IOException
    {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        System.out.println("Masukan Angka : ");
        String angka = br.readLine();
        int n = Integer.parseInt(angka);
        Soal_8 day4 = new Soal_8();
        day4.showData(n);
    }
	
	public void showData(int n)
    {
        String[][] data = setData(n);
        System.out.println("== Menampilkan data : "+ n +" ==");
       
        for(int y = 0; y < n; y++)
        {
            for(int x= 0; x < n; x++)
            {
                if(data[x][y] == null)
                {
                    data[x][y] = "\t";//x +"."+ y + 
                }
                System.out.print(data[x][y]);
            }
            
            System.out.println("\t");
        }
    }
	public int[] getAngka(int n)
    {
        int[] dtAngka = new int[n]; // batas nilai sebanyak nilai n
        //Get 

        for(int y = 0; y < n; y++)
        {
            if(y == 0 )
            	dtAngka[y]=1;
            else
            	dtAngka[y] = dtAngka[y - 1] + 1;
        }
        return dtAngka;
    }
	
	public String[][] setData(int n)
    {
        String[][] data= new String[n][n];
        int[] angka= getAngka(n);
        int max = n-1;
        int nt = max/2; //persegi panjang max*2
        for(int y =0; y < n; y++)
        {
        	int a=0;
            for(int x = 0; x < n; x++)
            {
                 if( (x-y<=nt) && (x+y >= nt) && (y-x <= nt) && (y+x <= max+nt) )//
                 {
                	data[x][y]=angka[a]+"\t";//angka[a]+
                	if(x<max/2)
                		a++;
                	else
                		a--;
                 }
                 
            }
            
        } 
		return data;
    }
}
