/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Day_3;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 *
 * @author asus
 */
public class Soal_2 {
public int[] setData(int n){ // Nilai Array
		int[] data = new int[n];
		//code set
		
		for(int y=0; y<n; y++)
		{
			for(int x=0; x<n; x++)
			{
                            if(x<3)
				data[x]= 1;
                            else
				data[x]=data[x-1]+data[x-2]+data[x-3];
			}
		}
		return data;
	}
	
	public void showData(int n){ // Hanya menampilkan
		System.out.println("== Menampilkan data : "+ n +" ==");
		int[] data = setData(n);
		
		for(int y=0; y<n; y++){
			for(int x=0; x<n; x++)
			{
				if(y==0)
				System.out.print(data[x]+ " ");
			}
			System.out.println();
		}
	}
	
	public static void  main(String ars[])throws Exception
	{ 
		InputStreamReader nilai = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(nilai);
		System.out.println("Masukan Angka : ");
		String angka = br.readLine();
		int n = Integer.parseInt(angka);
		
		Soal_2 logic = new Soal_2();
		logic.showData(n); // set nilai yang ditampilkan
	}    
}
