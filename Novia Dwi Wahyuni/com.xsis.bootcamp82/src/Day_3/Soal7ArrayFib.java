/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Day_3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author asus
 */
public class Soal7ArrayFib {
    public int[] getFib(int n){
        
        int[] data = new int[n];
       // int loop = 1;
        for(int y = 0; y < n; y++){
           
            if(y < 2){ 
                data[y] = 1;
            } else {
                data[y] = data[y - 1] + data[y - 2];
            }
            
            //loop++;
        }
        
        return data;
    }
    
    public String[][] setData(int n){
        String[][] data = new String[n][n];
        int[] fib1 = getFib(n);
        
       // for(int temp : fib1){
         //   System.out.println(""+ temp);
       // }
        
        int max = n - 1;
        
        for(int y = 0; y < n; y++){
            for(int x= 0; x < n; x++){
                
               if( y + x >= max && x <= y) {
                        //if (y%2==0){
                                data[x][y]=  fib1[n-y-1] + " ";
                        //}

                }
                else if(y+x<=max && x>=y) //Atas
                {
                        //if (y%2==0){
                                data[x][y]= fib1[y]+" ";
                       // }
                }
                else if(y+x<=max && x<=y) //Kiri
                {
                      //  if (x%2==0){
                                data[x][y]= fib1[x]+" ";
                      //  }
                }
                else if(y+x>=max && x>=y) //Kanan
                {
                        data[x][y]= fib1[n-x-1] + " ";
                } 
            }
        }
        
        
        return data;
    }
    
    public void showData(int n){
        System.out.println("== Menampilkan data : "+ n +" ==");
        String[][] data = setData(n);
        
        for(int y = 0; y < n; y++){
            for(int x= 0; x < n; x++){
                if(data[x][y] == null){
                    data[x][y] = "";
                }
                
                System.out.print(data[x][y]);
            }
            
            System.out.println("");
        }
        
    }
    
    public static void main(String[] args) throws IOException
    {
            InputStreamReader nilai = new InputStreamReader(System.in);
            BufferedReader br = new BufferedReader(nilai);
            System.out.println("Masukan Angka : ");
            String angka = br.readLine();
            int n = Integer.parseInt(angka);
            
            Soal7ArrayFib jawab = new Soal7ArrayFib();
            jawab.showData(7);
    }
}
